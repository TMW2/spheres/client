########################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Definitions - Prestart and post init sequences
image TMW2 = "gfx/logo.png"

label prestart:
    python:
        tr_busy=True
        tr_val=""
        tr_load=False
        tr_uptodate=False
        tr_memcheck=False
        tr_fatality=False
        TMP_MUSIC=get_sfx("bgm03")
        TMP_BACKG="battle"
        session_id=uuid.uuid4().hex
        config.predict_screens=False
        config.predict_screen_statements=False
        config.predict_file_pages=False
        config.predict_statements_callback=None
        config.predict_statements=0
        preferences.gl_framerate=30
        preferences.gl_powersave=True
    return

label splashscreen:
    show TMW2 at truecenter with fade
    pause 2.5
    hide TMW2 with Dissolve(1.5)
    python:
        """
    centered "{b}{color=#f00}Disclaimer{/color}\n\nBy running this software, you affirm that you have the right to do so.\nNo license is provided for using this demo software.\n\n\
\
\
{color=#f00}This client may contain assets which {u}does not{/u} belongs to it.\n\
If you are the legal owner of an asset, or their legal representant,\n\
And find an asset of yours illegally used, please, fill a DMCA complaint\n\
by sending an email to {a=mailto:dmca@tmw2.org}dmca@tmw2.org{/a}.{/color}\n\
\n\
\n\
No warranties.{/b}{fast}"
        """
    return

init 2 python:
    # Overlay for TERM signals
    def TermWatcher():
        global TERMINATE, CLOSING
        if TERMINATE and not CLOSING:
            stdout("TERM: TERMINATE RECEIVED")
            TERMINATE=False
            renpy.quit(relaunch=True)
        return

    config.overlay_functions.append(TermWatcher)

    # Do not attempt to predict screens: They may need unitialized data
    config.predict_screens=False
    config.predict_screen_statements=False
    config.predict_file_pages=False
    config.predict_statements_callback=None
    config.predict_statements=0


########################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Units upgrade and evolution
init python:
    def evocheck(level, unit):
        # Does an evolved form exist?
        #print("evocheck lv %d" % level)
        try:
            nu=unit["unit_id"]+1
            #print("nu is: %d" % int(nu))
            next_name=allunits[nu]["name"]
            #print("next name: %s" % next_name)
        except:
            return False

        # Level requeriment
        return level == unit["max_level"]

screen upgrade_char():
    vbox:
        xalign 0.5
        yalign 0.3

        # The unit
        hbox:
            add unitsquare(unit["unit_id"], czoom_70)
            null width 10
            label _("{size=32}{color=#fff}%s{/color}{/size}" % (unit["name"]))

        null height 20
        $i=0
        hbox:
            for ignored in enumerate(material):
                if i < 4:
                    imagebutton:
                        if material[i] >= 0:
                            idle unitsquare(Player["inv"][material[i]]["unit_id"], czoom_70)
                        else:
                            idle At("gfx/square/bg.png", czoom_70)
                        action Return(i)
                    $i+=1

        null height 20
        hbox:
            for ignored in enumerate(material):
                if i < 8:
                    imagebutton:
                        if material[i] >= 0:
                            idle unitsquare(Player["inv"][material[i]]["unit_id"], czoom_70)
                        else:
                            idle At("gfx/square/bg.png", czoom_70)
                        action Return(i)
                    $i+=1

        null height 80
        # The close button returns -1 and comes last (TODO)
        hbox:
            textbutton _("Merge") action Return(-2)
            null width 80
            textbutton _("Leave") action Return(-1)


screen evolve_char():
    vbox:
        xalign 0.5
        yalign 0.3

        # The unit
        hbox:
            add unitsquare(unit["unit_id"], czoom_70)
            null width 10
            add At("gfx/evol.png", czoom_70)
            null width 10
            add unitsquare(next["unit_id"], czoom_70)
            #label _("{size=32}{color=#fff}%s\n↓\n%s{/color}{/size}" % (unit["name"], next["name"]))

        null height 120
        $i=0
        hbox:
            xalign 0.5
            spacing 150
            for ignored in enumerate(material):
                if i < 2:
                    imagebutton:
                        if material[i] >= 0:
                            idle unitsquare(Player["inv"][material[i]]["unit_id"], czoom_70)
                        else:
                            idle At("gfx/square/bg.png", czoom_70)
                        action Return(i)
                    $i+=1

        null height 80
        # The close button returns -1 and comes last (TODO)
        hbox:
            xalign 0.5
            spacing 80
            textbutton _("Evolve") action [SensitiveIf(material[0]>=0 and material[1]>=0), Return(-2)]
            textbutton _("Cancel") action Return(-1)

label upgrade_pre:
    # who -> index. Set beforehand
    if who < 0:
        call screen msgbox("Error: Invalid upgrade parameters passed")
        return

    $ unit = allunits[Player["inv"][who]["unit_id"]]
    $ material = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]

    # Try to update inventory
    $ inv=get_inventory()
    python:
        try:
            renpy.call_screen("msgbox", "Error: %d" % int(inv))
        except:
            Player["inv"]=dlist()
            for a in inv:
                Player["inv"].append(a)

    jump upgrade

label upgrade:
    call screen upgrade_char()
    $message=_return
    if (message == -1):
        $who=-1
        return
        #jump restore
    if (message == -2):
        # TODO
        python:
            mats=list(set(material))
            mats.insert(0, who)
            if -1 in mats:
                mats.remove(-1)
            stdout(str(mats))
            ret=None
            ret=renpy.call_screen("confirm", "Are you sure you want to merge these units?", Return(True), Return(False))
            if ret:
                # TODO: DISPLAY ANIMATION
                message=send_packet("upgrade", str(mats))
                message=json_decode(message)
                try:
                    lv=Player["inv"][who]["level"]
                    narrator("Level up! %d -> %d" % (lv, int(message)+lv))
                except:
                    narrator("An error occured.\n\nError code: %s" % str(message))
                renpy.jump("upgrade_pre")
    else:
        $ cand=renpy.call_screen("inventory", True)
        if cand in material and cand >= 0:
            "This unit is already to be merged!"
        elif cand == who:
            "Cannot merge unit with itself!"
        else:
            # TODO: Check for party
            #"Trying to fuse [cand], must check if in party, duplicate, etc."
            $ material[message]=cand
        $ del cand

    # WIP
    jump upgrade


label evolve_pre:
    # who -> index. Set beforehand
    if who < 0:
        call screen msgbox("Error: Invalid upgrade parameters passed")
        return

    $ unit = allunits[Player["inv"][who]["unit_id"]]
    $ next = allunits[Player["inv"][who]["unit_id"]+1]
    $ material = [-1, -1]

    # Try to update inventory
    $ inv=get_inventory()
    python:
        try:
            renpy.call_screen("msgbox", "Error: %d" % int(inv))
        except:
            Player["inv"]=dlist()
            for a in inv:
                Player["inv"].append(a)

    #$renpy.call_screen("msgbox", "Feature not yet available\n%s" % next["name"])
    $renpy.hide("unit_"+str(unit["unit_id"]))
    jump evolve


label evolve:
    call screen evolve_char()
    $message=_return
    if (message == -1):
        $who=-1
        return
        #jump restore
    if (message == -2):
        # TODO
        python:
            mats=list(set(material))
            mats.insert(0, who)
            stdout(str(mats))
            ret=None
            ret=renpy.call_screen("confirm", "Are you sure you want to evolve this units?\nReagents will be lost forever!", Return(True), Return(False))
            if ret:
                message=send_packet("evolve", str(mats))
                message=json_decode(message)
                if message in ["200", 200]:
                    # TODO: DISPLAY ANIMATION
                    narrator("Evolution SUCCESS!")
                else:
                    narrator("Error code: 101!\n\n%s" % str(message))
                renpy.jump("inventory")
    else:
        $ cand=renpy.call_screen("inventory", True, 'alu["rare"] == %d and\
(\
 (alu["unit_id"] == %s) or\
 (alu["flags"] & UF_EVOMAT and alu["attribute"] == %d) or\
 (alu["flags"] & UF_SUPEREVO) )' % (unit["rare"], unit["unit_id"], unit["attribute"]))
        if cand in material and cand >= 0:
            "This unit is already to be used as material!"
        elif cand == who:
            "That's the unit being evolved!"
        else:
            # TODO: Check for party
            #"Trying to fuse [cand], must check if in party, duplicate, etc."
            $ material[message]=cand
        $ del cand

    # WIP
    jump evolve


########################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Player data displays
init python:
    def inventoryplace():
        global Player
        # Return next free slot index
        try:
            return Player["inv"].index(None)
        except:
            return len(Player["inv"])

    def get_inventory():
        raw=send_packet("get_inv", "")

        pt=json_decode(raw)
        if (pt == ERR_JSONDECODER):
            return ERR_JSONDECODER

        if (pt == FAILUREMSG):
            # TODO: msgbox you're offline, quit
            return ERR_LOGIN_DEFAULT

        print("get_inv(): "+str(pt))
        return pt

screen inventory(blank=False, filters="True"):
    # window
    # hbox
    vpgrid:
        cols 4
        spacing 5
        draggable True
        mousewheel True
        scrollbars "vertical"
        side_xalign 0.5
        xoffset 15
        #yoffset 45
        #xfill True
        yfill True

        # The close button returns -1 and comes first
        imagebutton:
                if not blank:
                    idle At("gfx/square/back_idle.png", czoom_70)
                    hover At("gfx/square/back_hover.png", czoom_70)
                else:
                    idle At("gfx/square/bg.png", czoom_70)
                action Return(-1)

        for i, item in enumerate(Player["inv"]):
            # We don't care for None items
            if item is not None:
                # Needed because eval :rolling_eyes:
                #$ ir=copy.copy(item["rare"])
                #$ print(str(locals()))
                python:
                    evl=False
                    #print("---- repr")
                    try:
                        alu=allunits[item["unit_id"]]
                    except:
                        alu={}
                        stdout("ERROR, alu: not defined, index %d" % i)
                    evl=eval(filters, globals(), locals())
                    #print(str(evl))
                    #print(str(filters))
                    #print(str(item))
                    #print(str(alu))
                if evl:
                    $ btn = unitsquare(item["unit_id"], czoom_70)
                    imagebutton:
                        idle btn
                        action Return(i)
                        #alternate "Show the char data chart"

screen char_details(un, hpval, akval, idx):
    style_prefix "confirm"

    frame:
        #at msgbox_emp
        yalign 0.95

        vbox:
            xalign 0.5
            yalign 0.95
            spacing 30

            label _("{b}%s{/b}\n%s\n\n{i}%s{/i}\nHP: %d — ATK: %d\nLv %d/%d — EXP: %d") % (
                                    star_write(un["rare"]), un["name"],
                                    parse_job(un["job"]),
                                    hpval, akval,
                                    Player["inv"][idx]["level"],
                                    un["max_level"],
                                    Player["inv"][idx]["exp"]):
                style "confirm_prompt"
                xalign 0.5

            hbox:
                xalign 0.5
                spacing 100

                textbutton _("Merge") action [SensitiveIf(Player["inv"][idx]["level"] < un["max_level"]), Return(-1)]
                textbutton _("Evolve") action [SensitiveIf(evocheck(Player["inv"][idx]["level"], un)), Return(-2)]

            hbox:
                xalign 0.5
                spacing 100

                textbutton _("Ok") action Return(0)

    ## Right-click and escape answer "no".
    key "game_menu" action Return(0)


# Show inventory button
label inventory:
    play music MUSIC_PARTY.id() fadein 0.5
    $ hud_clear()

    # Try to update inventory
    $ inv=get_inventory()
    python:
        try:
            renpy.call_screen("msgbox", "Error: %d" % int(inv))
        except:
            Player["inv"]=dlist()
            for a in inv:
                Player["inv"].append(a)

label show_inv:
    call screen inventory
    if (_return >= 0):
        $stdout("Selected unit index %d" % _return)
        $un=allunits[Player["inv"][_return]["unit_id"]]
        $show_img("unit_"+str(un["unit_id"]), at_list=[ttop]) # truecenter
        $hpval=readjust_status(Player["inv"][_return]["level"], un["hp"])
        $akval=readjust_status(Player["inv"][_return]["level"], un["strength"])

        $ret=renpy.call_screen("char_details", un, hpval, akval, _return)

        # Proccess input
        if ret == -1:
            $who=_return
            call upgrade_pre
        elif ret == -2:
            $who=_return
            call evolve_pre

        $renpy.hide("unit_"+str(un["unit_id"]))
    else:
        jump restore

    jump show_inv


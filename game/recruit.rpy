########################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Recruits
init python:
    import json
    def recruit(t, a):
        global Player
        raw=send_packet("recruit", recruitdata(t, a))

        rc=json_decode(raw)
        try:
            return int(rc)
        except:
            pass

        # Update data
        try:
            Player[rc["currency"][0]]=rc["currency"][1]
        except:
            renpy.notify("ERROR, Currency is undefined.")
            stdout("ERROR, Undefined currency")

        for unit in rc["units"]:
            _window_hide(None)
            if (debug):
                print(str(unit))
            idx=inventoryplace()
            unit["unit_id"]=int(unit["unit_id"])
            if (debug):
                stdout("Recruit index: %d" % idx)
                print("Player inventory: %s" % str(Player["inv"]))
                print("Unit ID: %s" % str(unit["unit_id"]))
                print("Unit Name: %s" % str(allunits[int(unit["unit_id"])]["name"]))
                print("Unit Rarity: %d" % allunits[int(unit["unit_id"])]["rare"])
            Player["inv"][idx]=unit
            txt=allunits[int(unit["unit_id"])]["name"]
            rar=allunits[int(unit["unit_id"])]["rare"]
            star=star_write(rar)
            sprite=str(unit["unit_id"])
            show_img("unit_"+sprite, at_list=[truecenter])
            renpy.pause(0.1)
            try:
                renpy.call_screen("msgbox", "%s\n\nRecruited %d★ %s" % (star, rar, txt), autoclose=6.0)
            except:
                pass
            renpy.hide("unit_"+sprite)
            _window_show(None)

        # Return result
        if (rc["code"] == ERR_NOGEMS):
            renpy.notify("You don't have enough gems to complete.")
        if (rc["code"] == ERR_INVFULL):
            renpy.notify("You don't have enough space to complete!")

        return rc["code"]


label tavern:
    $ show_img("bg tavern", False) # Validate
    scene bg tavern
    play music MUSIC_PARTY.id() fadein 0.5
    window hide
    #alltaverns

    jump tv_loop

screen tavern():
    default tavern = 0
    fixed:
        #xmargin 50
        #ymargin 50
        #xalign 0.5
        #ypos 0.9
        #yanchor 1.0
        xfill True
        yfill True
        frame:
          background "gui/tv_frame.webp"
          xalign 0.5
          #yanchor 1.0
          #ypos 0.9
          yalign 0.5
          xmaximum 615
          xminimum 615
          ymaximum 1000
          yminimum 1000
          vbox:
            box_wrap True
            xoffset 50
            yoffset 50

            if alltaverns[tavern]["banner"] != "":
                label _(".:: Special Summon ::.")
                add "banner_%s" % alltaverns[tavern]["banner"]
            else:
                pass

            showif (alltaverns[tavern]["min_quest"] <= Player["quest"]):
                label alltaverns[tavern]["details"]
                null:
                    height 32
                label _("%d %s the draw!" % (alltaverns[tavern]["price"], alltaverns[tavern]["currency"]))
            else:
                label _("This tavern hasn't been unlocked yet.")
                null height 64
                label _("Unlocks at quest %d" % alltaverns[tavern]["min_quest"])

        vbox:
            box_wrap True
            xoffset 50
            yoffset 50
            ypos (1312-42)
            yanchor 1.0
            xpos 255
            xanchor 0.0
            hbox:
              spacing 24

              showif alltaverns[tavern]["min_quest"] >= 0 and alltaverns[tavern]["min_quest"] <= Player["quest"]:
                button:
                    xmaximum 250 #350
                    yminimum 60  #111
                    ymaximum 110 #111
                    idle_background Frame("gui/green_btn.png", 60, 30)
                    hover_background Frame("gui/green_btn_hover.png", 60, 30)
                    action Return([tavern, 1])
                    hbox:
                        yoffset 10
                        null width 25
                        text _("Summon %d" % 1):
                            color "#001864"
                            size 24
                            outlines [(1,"#FFF",0,0)]
                        null width 25
                button:
                    xmaximum 250 #350
                    yminimum 60  #111
                    ymaximum 110 #111
                    idle_background Frame("gui/green_btn.png", 60, 60)
                    hover_background Frame("gui/green_btn_hover.png", 60, 60)
                    action Return([tavern, alltaverns[tavern]["min_draws"]])
                    hbox:
                        yoffset 10
                        null width 25
                        text _("Summon %d" % alltaverns[tavern]["min_draws"]):
                            color "#001864"
                            size 24
                            outlines [(1,"#FFF",0,0)]
                        null width 25
              elif (alltaverns[tavern]["min_quest"] < 0):
                label _("Coming soon!")
              else:
                pass
            null:
                height 42
            hbox:
                button:
                    xmaximum 250 #350
                    yminimum 60  #111
                    ymaximum 110 #111
                    idle_background Frame("gui/green_btn.png", 60, 60)
                    hover_background Frame("gui/green_btn_hover.png", 60, 60)
                    action Return([-1, -1])
                    hbox:
                        yoffset 10
                        null width 25
                        text _("Return"):
                            color "#001864"
                            size 24
                            outlines [(1,"#FFF",0,0)]
                        null width 25
                null width 32
                showif tavern > 0:
                    button:
                        xmaximum 250 #350
                        yminimum 60  #111
                        ymaximum 110 #111
                        idle_background Frame("gui/purple_btn.png", 60, 30)
                        hover_background Frame("gui/purple_btn_hover.png", 60, 30)
                        action SetScreenVariable("tavern", tavern-1)
                        hbox:
                            yoffset 5
                            null width 25
                            text _("«"):
                                color "#FFF"
                                size 24
                                outlines [(1,"#000",0,0)]
                            null width 25
                showif len(alltaverns) > tavern+1:
                    button:
                        xmaximum 250 #350
                        yminimum 60  #111
                        ymaximum 110 #111
                        idle_background Frame("gui/purple_btn.png", 60, 30)
                        hover_background Frame("gui/purple_btn_hover.png", 60, 30)
                        action SetScreenVariable("tavern", tavern+1)
                        hbox:
                            yoffset 5
                            null width 25
                            text _("»"):
                                color "#FFF"
                                size 24
                                outlines [(1,"#000",0,0)]
                            null width 25

label tv_loop:
    # Download all tavern banners
    # TODO: Make a copy() and remove locked taverns (min_quest > Player["quest"])
    python:
        tr_loading = True
        for tv in alltaverns:
            renpy.pause(0.001, hard=True)
            if tv["banner"] != "":
                show_img("banner_"+tv["banner"], False)
        tr_loading = False

    call screen tavern

    if (_return[0] < 0 or _return[1] < 1):
        jump restore

    $ message=recruit(_return[0], _return[1])

    if (message == OFFLINEMSG):
        "Server replies:" "[message]\n\nYou are offline?"
        return

    jump tv_loop


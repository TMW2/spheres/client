########################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Player data displays
init python:
    # Function which list all unit id on party
    def party_dupcheck(pid):
        ar=[]
        for unt in Player["party_%s" % pid]:
            ar.append(unt["unit_id"])
        return ar

    def get_party(pid, internal=False):
        raw=send_packet("get_party", str(pid))

        pt=json_decode(raw)
        if (pt == ERR_JSONDECODER):
            return ERR_JSONDECODER

        Player["party_%d" % pid]=pt
        if (internal):
            return False

        allmem=""
        for member in pt:
            # Skip'invalid members
            if member["unit_id"] <= 0:
                continue

            # TODO: show image
            rfc=Player["inv"][member["inv_id"]]

            allmem+="%d★ %s (Lv %d, %d exp)\n" % (allunits[member["unit_id"]]["rare"], allunits[member["unit_id"]]["name"], rfc["level"], rfc["exp"])
            #Player["party_1"].append(member)

        renpy.call_screen("msgbox", "Party %d\n %s" % (pid, allmem))
        return True

    def set_party(pid, members):
        raw=send_packet("set_party", """{"party_id": %d, "formation": %s}""" % (pid, str(members)))

        pt=json_decode(raw)
        if (pt == ERR_JSONDECODER):
            return ERR_JSONDECODER

        return True

    def party_update(pid, pix):
        newindex=renpy.call_screen("inventory", True)

        # We only need inventory id's
        # So make a Party variable with this
        Party=dlist()
        for une in Player["party_%d" % pid]:
            Party.append(une["inv_id"])

        if debug:
            print(str(Party))

        # Add unit, but only if no duplicates are found
        # Exception: If it is -1, then we can add
        if newindex != -1:
            dupcheck=party_dupcheck(pid)
            dupid=Player["inv"][newindex]["unit_id"]
            if dupid in dupcheck:
                renpy.notify(_("You cannot add duplicate party members!"))
            else:
                Party[pix]=newindex
        else:
            Party[pix]=newindex

        if debug:
            print(str(Party))

        # Set party
        set_party(pid, Party)
        return True

screen party_main():
    default party = 1
    vbox:
        xalign 0.5
        yalign 0.3
        label _("{size=32}{color=#fff}Party %d{/color}{/size}" % (party))
        null height 20
        hbox:
            for i, item in enumerate(Player["party_%d" % party]):
                imagebutton:
                    if item["unit_id"] > 0:
                        idle unitsquare(item["unit_id"], czoom_70)
                    else:
                        idle At("gfx/square/bg.png", czoom_70)
                    action Return([party, i])
        null height 80
        # The close button returns -1 and comes last (TODO)
        imagebutton:
                idle At("gfx/square/back_idle.png", czoom_75)
                hover At("gfx/square/back_hover.png", czoom_75)
                action Return(-1)

label party_lobby:

    call screen party_main

    # Return to town
    if _return == -1:
        jump restore

    # Update party index
    $party_update(_return[0], _return[1])
    $ get_party(_return[0], True)

    jump party_lobby



label party_lobby_enter:
    play music MUSIC_PARTY.id() fadein 0.5
    $ hud_clear()

    # Try to update inventory
    $ inv=get_inventory()
    python:
        try:
            renpy.call_screen("msgbox", "Error: %d" % int(inv))
        except:
            Player["inv"]=dlist()
            for a in inv:
                Player["inv"].append(a)

    # FIXME
    $ get_party(1, True)
    jump party_lobby


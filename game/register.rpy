########################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Account login & registration (screen & labels)
screen welcome():
    #window:
        #background Frame("gui/frame.png", 5, 5)
        #yalign 0.5
        #xalign 0.5
        #ypadding 30
        #xpadding 30
        #xmaximum 0.7
        #xminimum 0.7
        #yminimum 95
        #vbox:
            #text (_("Welcome to %s!" % (config.name))) color "#fefefe" yalign 0.0 xalign 0.5 size 32
            #null height 0.2
    fixed:
        image "gfx/nourishedflower.webp":
            xalign 0.5
            yalign 1.0

        label (_("Welcome to %s!" % (config.name))):
            style "pref_label"
            yalign 0.02
            xalign 0.50
            text_size 54
            text_outlines [ (1, "#000", 0, 0) ]
            #color "#fefefe"
            #size 32

        imagebutton auto "gfx/gui/cog_%s.png":
            xalign 1.0
            yalign 1.0
            action ShowMenu('preferences')

        textbutton _("{size=36}Login{/size}"):
            xalign 0.33
            yalign 0.15
            xpadding 30
            ypadding 30
            background Frame("gui/frame.png", 5, 5)
            action Jump("register_password")

        textbutton _("{size=36}Register{/size}"):
            xalign 0.67
            yalign 0.15
            xpadding 30
            ypadding 30
            background Frame("gui/frame.png", 5, 5)
            action Jump("register_email")

        textbutton _("{size=36}Guest Session{/size}"):
            xalign 0.5
            yalign 0.25
            xpadding 30
            ypadding 30
            background Frame("gui/frame.png", 5, 5)
            action Jump('register_bypass')

############################################################################
# Registration procedures
# You don't have an account yet
label register_email:
    python:
        import re
        regex = '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$'
        email = ""
        # Ask player for their email
        while email == "":
            email=renpy.call_screen("input_box", "Please insert your {a=about_email}email{/a} to register an account: \n", "Welcome to %s!" % (config.name))
            if not re.search(regex, email):
                email=""
                renpy.call_screen("msgbox",
                                  "The email you've entered is not valid.")

        # You've inserted a valid email
        raw=send_packet("register", """{"email": "%s", "MyUID": "%s"}""" % (email, persistent.MyUID))
        bt=json_decode(raw)
        # TODO: Maybe just use uuid4 to make a fake email, then fetch password
        # Show player the fake email (UUID) and the password and disclaim:
        # THIS WON'T BE SHOWN AGAIN, STORE IT SOMEWHERE SECURE
        # (or save to persistent data and allow linking later?)
        # ...Actually the email/UUID gimmick is useless?
        # Also, couldn't people just brute-force other ppl passwords?

        try:
            password=bt["password"]
            valid=True
        except:
            traceback.print_exc()
            # Either a SQL error, or a server error, or a connection error...
            # But either way, we can't proceed!
            renpy.call_screen("msgbox", "An error happened, maybe this email is already registered.\nPlease try again later.")
            valid=False

    # An error happened, return to login screen
    if not valid:
        jump start

    # Save data
    $ persistent.password=password
    jump login

# Registration procedures
# You already have an account and want to recover it
label register_password:
    python:
        password=""
        while password == "":
            password=renpy.call_screen("input_box",
                "Welcome to %s!\nPlease insert your password: " % (config.name))
            if not password.isalnum():
                renpy.call_screen("msgbox",
                                  "The password you've entered is not valid.")
                password=""
    jump login

label about_email:
    $ renpy.call_screen("msgbox", "Your account password will be emailed to you. This is the only way to recover a lost account. You can use an {a=https://www.tempmailaddress.com/}Temporary email{/a} if you wish.", False)
    return

# Registration procedures
# You don't want an account, we'll just use the MyUID instead.
label register_bypass:
    python:
        raw=send_packet("register", """{"email": "", "MyUID": "%s"}""" % (persistent.MyUID))
        bt=json_decode(raw)
        # TODO: Allow linking later (it's already implemented server-side)
        # FIXME: Security disclosure: MyUID is shared between servers
        # If you are using it instead of linking, a server admin could steal
        # all accounts you made on other servers. ALWAYS link your account
        # (Low priority, there's only one official server atm)

        try:
            password=bt["password"]
            valid=True
        except:
            traceback.print_exc()
            # Either a SQL error, or a server error, or a connection error...
            # But either way, we can't proceed!
            renpy.call_screen("msgbox", "An error happened, maybe you already have a guest account and need to use email linking.\nPlease try again later.")
            valid=False

    # An error happened, return to login screen
    if not valid:
        jump start

    # Save data
    $ persistent.password=password
    jump login

    return


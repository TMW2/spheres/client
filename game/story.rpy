#################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#################################################################################
# Game Story labels
define e = Character("Player")
define you = Character("[persistent.nickname]")

define e_dt = Character("???", color="#cccccc")
define e_dtk = Character("Inspector", color="#cccccc")
define e_ds = Character("???", color="#8e96d3")
define e_dsk = Character("Deadly Shadow", color="#8e96d3")
define e_mn = Character("Mentor", color="#ffccaa")
define e_gd = Character("???", color="#9943a8")
define e_gdk = Character("Goodess", color="#9943a8")
define e_gm = Character("Grandmaster", color="#9b7436")

define e_ot = Character("???", color="#ccdddd")
define e_gg = Character("Giulio", color="#ffaacc")

label SQ00001_prologue:
    python:
        while persistent.nickname is None:
            nick=renpy.call_screen("input_box",
    "Please insert an unique nickname (max 8 alphanumeric chars): ")
            if len(nick) > 8 or not nick.isalnum():
                renpy.call_screen("msgbox", "ERROR\nERRONEOUS NICKNAME DETECTED")
            else:
                persistent.nickname=nick

    play music MUSIC_PROLOGUE01.id() fadein 0.5
    scene black with None
    window hide
    centered "{cps=36}{b}Somewhere distant{/b}{/cps}{w=2.0}{nw}"
    scene bg darklands with Dissolve(2.0)
    centered "{b}Somewhere distant{/b}\n\n{fast}{cps=36}Darklands{/cps}"
    show dialog_deadlyshock at toffscreenright with None
    show dialog_darkmana at toffscreenright with None
    window show
    show dialog_detective at tcenter with Dissolve(1.0)
    e_dt "..."
    e_dt "This high peak... This eerie atmosphere... And, ugh, this smell..."
    e_dt "So, these are the darklands."
    e_dt "I didn't realize they were real. Or at least, I hoped so. If it is here..."
    e_dt "...Then there can be only one meaning. I have to report this at once!"
    e_ds "Going somewhere, darling? ♪"
    show dialog_detective at prologue_right1
    show dialog_deadlyshock at prologue_right2
    with move
    e_dt "Uh oh..."

    show dialog_detective at prologue_center
    show dialog_deadlyshock at prologue_right2
    show dialog_darkmana at prologue_left
    with move
    e_ds "Humans, always in hurry... Why don't you dance with us, darling? ♪"
    e_dt "I appreciate the invite, but I cannot afford to fail this mission. En garde!"
    scene black with vpunch
    stop music fadeout 2.5
    pause 1.0
    centered "Meanwhile\n{w=0.5}{nw}"
    centered "Meanwhile\nAcademy of Weisheit"
    scene bg academy with Dissolve(0.5)
    show dialog_spearman at toffscreenright with None
    play music MUSIC_PROLOGUE02.id() fadein 1.0

    "{color=#0f0}*bustling noises*{/color}"

    you "It surely is crowded here today! Who would think so many people would show up..."
    show dialog_giulio at tcenter with Dissolve(0.5)
    e_gg "OF COURSE so many people would show up, [persistent.nickname]. {i}He{/i} is a seasoned war veteran!"
    
    you "I would not know that, Giulio. You know how much time I've spent studying magic!"

    e_gg "Yes, you Mana Mages are the strangest kind of mages I ever heard about."

    e_gg "It might be easy for those like you, who can control the Mana Spheres..."

    you "Excuse me?! I barely can bring them forth, let alone controlling them."

    show dialog_spearman at prologue_right2
    show dialog_giulio at tleft
    with move

    e_ot "Excuse me, are you both enrolled at the Weisheit Academy?"
    e_gg "Uhm... You are an imperial spearman, right? What... Are you doing here?"
    you "Actually, Giulio... When did everyone left??"
    play music MUSIC_PROLOGUE03.id() fadein 0.6
    e_ot "Oh great, you {i}are{/i} students! I was afraid of returning to the base without any kills."
    e_ot "Die for me!!"

    e_gg "{color=#0f0}*psst*{/color} [persistent.nickname], be careful. You are no match for an imperial spearman, leave combat for a top grade knight like myself."

    # Giulio joins you!
    window hide
    python:
        txt=allunits[10000000]["name"]
        rar=allunits[10000000]["rare"]
        star=star_write(rar)
        show_img("unit_10000000", at_list=[truecenter])
        renpy.pause(0.1)
        renpy.call_screen("msgbox", "%s\n\nRecruited %d★ %s" % (star, rar, txt))
        renpy.hide("unit_10000000")
        _window_show(None)



    pause 0.2
    e_gg "You just focus at giving me spheres to fight. If you {color=#0f0}swipe my card down{/color}, I'll use the sphere there against these enemies."
    e_gg "I know you cannot control much beyond that right now, but that will suffice to defeat this spearman."
    e_gg "When you click on {color=#0f0}End turn{/color} at the upper right corner, we will fight. There are other things a mana mage can do, like summons or allowing us to use skills, but don't worry with that now."

    show dialog_giulio at prologue_left
    show dialog_spearman at tcenter
    with move
    e_gg "Let's do it! It is our time to shine!"
    return

####################################################
label SQ00001_post:
    window show
    scene bg forest_sunset
    show dialog_mentor at tcenter
    with fade
    e_mn "What are you both still doing here?! Can't you see the empire is upon us?!"
    you "Uhm, we were distracted..."
    e_mn "Now, you are a mana mage, right? Don't pretend to be a fool."
    e_mn "Take these crystals and try summoning someone to help you."
    e_mn "There should be a stronghold if you go though the meadow path, bordering the forest of fae."
    e_mn "The stronghold is much safer, go there and regroup with other students. I must go now."
    hide dialog_mentor with dissolve
    you "That was a lot to understand at once."
    show dialog_giulio at tcenter with dissolve
    e_gg "I can't believe you. It was {i}him{/i}, the marshall who wiped out an enemy platoon with just tactics."
    e_gg "He told you to {color=#0f0}recruit someone at the Tavern{/color} and head to the stronghold, right? Let's go!"
    scene black
    show expression Text(_("{font=f/Datalegreya.otf}{color=#fff}Act I:\n\n{/color}{/font}"), size=54) at truecenter
    show expression Text(_("{font=f/Datalegreya.otf}{color=#fff}\n\nThe Empire Invades{/color}{/font}"), size=54) at truecenter
    show expression Text(_("{font=f/Datalegreya.otf}Mana Spheres{/font}"), size=27)
    with Dissolve(1.5)
    window hide
    pause 2.5
    scene black with Dissolve(1.5)
    pause 0.3
    return

####################################################
label SQ00005_post:
    $ show_img("bg humancity", False)
    window show
    scene bg humancity
    show dialog_giulio at tcenter
    with Dissolve(1.0)
    e_gg "The stronghold is just ahead. It won't be long, now."
    return

####################################################
label SQ00006_pre:
    $ show_img("bg humancity", False)
    $ show_img("dialog_footsoldier", False)
    scene bg humancity with Dissolve(1.0)
    "You" "We did it! We reached the Stronghold!"
    show dialog_mentor at tcenter
    with dissolve
    e_mn "Yes, and you {i}are{/i} late. The council session has already started."
    window show
    scene bg castle2
    show dialog_goodess at tcustom(-0.8)
    show dialog_mentor at tcustom(-2.0)
    show dialog_grandmaster at tcustom(1.4)
    show dialog_giulio at tcustom(1.5)
    with Dissolve(1.0)
    e_gm "...And that's our current situation. Any questions? No? Good, because we don't have time to explain."
    e_gm "For those whom arrived {i}late{/i}, you can catch up later. The Empire will soon be upon us. Dismissed!"
    hide dialog_grandmaster with dissolve
    show dialog_mentor at tcustom(-1.5) with move
    e_gd "Welcome, [persistent.nickname]. I've heard you're a novice Mana Mage? Unfortunately, not many of your kind is around."
    e_gd "I wish I could stay and chat, but the enemy is on our doors. I advise you to get ready as well."
    hide dialog_goodess with dissolve
    e_mn "Giulio, [persistent.nickname]. We are planning an expedition to ask for aid from our allies at the Forest of Fae."
    e_mn "However, if this stronghold falls now, it'll be all for naught. And I fear our enemy already arrived."

    scene bg humancity
    show dialog_grandmaster2 at tzoom(0.70), tcustom(0.1)
    show dialog_footsoldier as sp0 at tcustom(-1.5)
    show dialog_footsoldier as sp1 at tcustom(-1.4)
    show dialog_footsoldier as sp2 at tcustom(-1.3)
    show dialog_footsoldier as sp3 at tcustom(-1.2)
    show dialog_footsoldier as sp4 at tcustom(-1.1)
    show dialog_footsoldier as sp5 at tcustom(-1.0)
    show dialog_footsoldier as sp6 at tcustom(-0.9)
    show dialog_footsoldier as sp7 at tcustom(-0.8)
    show dialog_footsoldier as sp8 at tcustom(-0.7)
    show dialog_footsoldier as sp9 at tcustom(-0.6)
    show dialog_mentor at tcustom(1.2)
    with Dissolve(1.5)
    e_mn "Defeat enemies to {color=#0f0}fill the summon gauge{/color}, then click the summon button to unleash a powerful attack."
    show dialog_grandmaster2 at tzoom(0.70), tcustom(0.3)
    show dialog_footsoldier as sp0 at tcustom(-1.4)
    show dialog_footsoldier as sp1 at tcustom(-1.3)
    show dialog_footsoldier as sp2 at tcustom(-1.2)
    show dialog_footsoldier as sp3 at tcustom(-1.1)
    show dialog_footsoldier as sp4 at tcustom(-1.0)
    show dialog_footsoldier as sp5 at tcustom(-0.9)
    show dialog_footsoldier as sp6 at tcustom(-0.8)
    show dialog_footsoldier as sp7 at tcustom(-0.7)
    show dialog_footsoldier as sp8 at tcustom(-0.6)
    show dialog_footsoldier as sp9 at tcustom(-0.5)
    with move
    e_mn "The summons act first. Make sure the timing is right, they can change the tides of the battle."
    show dialog_grandmaster2 at tzoom(0.70), tcustom(0.5)
    show dialog_footsoldier as sp0 at tcustom(-1.3)
    show dialog_footsoldier as sp1 at tcustom(-1.2)
    show dialog_footsoldier as sp2 at tcustom(-1.1)
    show dialog_footsoldier as sp3 at tcustom(-1.0)
    show dialog_footsoldier as sp4 at tcustom(-0.9)
    show dialog_footsoldier as sp5 at tcustom(-0.8)
    show dialog_footsoldier as sp6 at tcustom(-0.7)
    show dialog_footsoldier as sp7 at tcustom(-0.6)
    show dialog_footsoldier as sp8 at tcustom(-0.5)
    show dialog_footsoldier as sp9 at tcustom(-0.4)
    with move
    e_mn "Now - {i}en garde!{/i}"
    return


####################################################
label SQ00007_post:
    $ show_img("summon_1", False)
    scene bg castle2
    show summon_1 at truecenter
    with Dissolve(1.5)
    "???" "We saw your thirst. We saw your fight. We... Approve of you."
    "???" "We are the lurking danger, the shadows which prey on our foes. And we shall be your blade, if you desire."
    "???" "{color=#0f0}Summon us{/color} at combat by {color=#0f0}tapping our arrow{/color} on the summoning screen."
    you "I believe the summon configuration screen was some sort of cog on the battle? I should try this later."
    $ Player["max_sum"] = max(Player["max_sum"], 1) # FIXME
    scene black with dissolve
    return


####################################################
label SQ00010_post:
    window show
    scene bg castle2
    show dialog_goodess at tcustom(-0.8)
    show dialog_mentor at tcustom(-2.0)
    show dialog_grandmaster at tcustom(1.4)
    show dialog_giulio at tcustom(1.5)
    with Dissolve(1.0)
    e_gm "Hmpf. We survived. For now. But the empire is sure to strike back."
    e_gm "[persistent.nickname] wasn't bad either. For a newbie. Either way, if we stay here idle it is a matter of time until we fall."
    e_gm "Which is why you will be asking for the help of the elves. They do not like trespassers, so expect to be attacked by them. Dismissed!"
    hide dialog_grandmaster with dissolve
    show dialog_mentor at tcustom(-1.5)
    show dialog_goodess at tcustom(-0.6)
    with move
    e_gd "This is an important task, and the journey is long. Please stay safe."
    e_mn "You also gained access to this stronghold tavern. {color=#0f0}Recruit more allies{/color} from this new tavern if you have the chance."
    e_gd "We will build barricades and send more emissaries to other fronts on the meanwhile, so do not worry with us."
    show dialog_goodess at tcustom(3.0)
    show dialog_mentor at tcustom(3.0)
    with move
    hide dialog_goodess with None
    hide dialog_mentor with None
    e_gg "A waste of my skills, but could be worse. Fighting with the Empire was getting boring."
    e_gg "By the way, did you noticed the {color=#0f0}colored orbs{/color} near the enemies? That is their element."
    e_gg "We are going to face a lot of nature (green) enemies; So a party with fire (red) allies will be helpful."
    e_gg "Just be careful, the boss usually is a nasty surprise with a totally different element."
    show dialog_giulio at tcustom(-2.0)
    with move
    hide dialog_giulio with None
    you "{color=#0f0}*sigh*{/color} I just wanted to graduate peacefully, and now I am an emissary. Still, I hope for a safe travel."
    scene black with dissolve
    centered "However, [persistent.nickname] did not knew that this was only the begin of their journey.\n{p}Meanwhile, lurking in the shadows, something much bigger was at play..."
    scene black
    show expression Text(_("{font=f/Datalegreya.otf}{color=#fff}Act II:\n\n{/color}{/font}"), size=54) at truecenter
    show expression Text(_("{font=f/Datalegreya.otf}{color=#fff}\n\nAmbushes and Travelers{/color}{/font}"), size=54) at truecenter
    show expression Text(_("{font=f/Datalegreya.otf}Mana Spheres{/font}"), size=27)
    with Dissolve(1.5)
    window hide
    pause 2.5
    scene black with Dissolve(1.5)
    pause 0.3
    return

####################################################
label SQ00011_post:
    scene bg forest_sunset
    you "...I thought the forest was closer."
    e_gg "Are you blind? Did you saw any forest while at the stronghold? Of course we have to walk."
    e_gg "I think we'll face a garrison before we enter the forest, then we'll have nature enemies to worry about."
    return

####################################################
label SQ00015_pre:
    scene bg forest_sunset
    e_gg "Watch out. This is the forest garrison. Once we pass it, we'll be in elfic territory."
    e_gg "Let's try to rush through, I don't think it is heavily guarded."
    return

####################################################
label SQ00016_pre:
    ## TODO FIXME old scripts
    scene black with dissolve
    centered "{color=#f00}The content past this point is old and might not work. You have been warned.{/color}"
    ## TODO FIXME old scripts
    ## PS. They also need better spacing

    #$ show_img("bg wilderness", False)
    #scene bg wilderness with Dissolve(1.0)
    $ show_img("dialog_milleue")
    "Milleue" "Are you the ones assigned to the Forest of Fae bandits? Am I right?"
    "Giulio" "And you would be...?"
    "Milleue" "So I'm right! Forest of Fae is sacred. We the Empire won't let you go there idly."
    "Milleue" "If you can, however, defeat my unit, then I'll reconsider your worthiness."
    "Milleue" "Perhaps I might even join you. Now, to arms! I'll judge you myself!"
    return

####################################################
label SQ00016_post:
    #$ show_img("bg wilderness", False)
    #scene bg wilderness with Dissolve(1.0)
    $ show_img("dialog_milleue", at_list=[tcenter])
    "Milleue" "Alright, I surrender! I admit defeat."
    "Milleue" "I'm afraid the path to the Forest of Fae won't be a walk in the park."
    "Milleue" "I'll go with you. But the other Imperial Soldiers will still try to stop you as you go."
    "You" "Wait. I always thought the Marshall served the empire...?"
    "Milleue" "Politics."
    "You" "Oh right. I guess I won't ask more, then."
    return

####################################################
label SQ00017_post:
    scene bg forest_sunset with Dissolve(0.5)
    "Giulio" "Mother Nature's sent us wolves... How much I hate it."
    "Giulio" "Mother Nature, please, GIVE ME A DECENT CHALLENGE!"
    $ show_img("dialog_milleue", at_list=[tcenter])
    "Milleue" "Be careful with what you wish for."
    "Giulio" "Oh, please. My sword will get rusty at this small fry. These wolves aren't qualified for a top knight like myself."
    return

####################################################
label SQ00018_pre:
    scene bg forest_sunset with Dissolve(0.5)
    "Giulio" "I hear battle sounds up ahead."
    "You" "It'll be quite a detour, but we should check."
    $ show_img("dialog_bandit", at_list=[tleft], tag="l")
    $ show_img("dialog_princess", at_list=[tright], tag="r")
    "{b}.:: CONTINUES ::.{/b}"
    return

####################################################
label SQ00019_post:
    scene bg forest_sunset with Dissolve(0.5)
    $ show_img("dialog_princess", at_list=[tcenter])
    "{b}.:: CONTINUES ::.{/b}"
    return




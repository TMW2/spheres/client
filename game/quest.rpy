########################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Mission Selection & Quest Handling
init python:
    ######################## Server Communications
    def loadquest(quest_id, party_id=1):
        raw=send_packet("begin_quest", questdata("Main", quest_id, party_id))

        bt=json_decode(raw)
        if (bt == ERR_JSONDECODER):
            return ERR_JSONDECODER
        return bt

    def loadbattle(new_arrange):
        global Battle
        raw=send_packet("battle", battledata(new_arrange, Battle["spheres"], use_sphere))

        bt=json_decode(raw)
        if (bt == ERR_JSONDECODER):
            return ERR_JSONDECODER
        return bt

    def loadsummon():
        global Battle
        # FIXME: Retrieve summon ID, etc
        raw=send_packet("summon", "[%d]" % persistent.summon)

        bt=json_decode(raw)
        if (bt == ERR_JSONDECODER):
            return ERR_JSONDECODER
        return bt

###################################################################
# TODO: Quest menus and selections should be auto-generated
# World map structure: ["name", min_level, {quest1}, {quest2} ...]
label quest_select:
    $ show_img("bg battle2", False)
    scene bg battle2
    play music MUSIC_WORLDMAP.id() fadein 0.5
    python:
        worldmap=[]
        areamap=[]
        for arx in allworld["Main"]:
            arena=copy.copy(arx)
            name=arena.pop(0)
            req=arena.pop(0)
            if Player["quest"] >= req:
                worldmap.append((name, arena))

        # Display world map menu
        worldmap.append(("Return", -1))
        mapselected=renpy.display_menu(worldmap)
        del worldmap
        if mapselected == -1:
            renpy.jump("restore")

        # Now we have the mapselected array, with dict
        sfx=mapselected.pop(0)
        TMP_MUSIC=get_sfx(sfx)
        #renpy.play
        for arx in mapselected:
            quest=copy.copy(arx)
            name=quest["name"]
            qid=quest["quest_id"]

            # We also want to show cost and requeriments
            entry=dl_search(allquests["Main"], "quest_id", qid)
            if entry != ERR_INVALID:
                cost=entry["cost"]
                req=entry["requeriment"]
            else:
                cost=-1
                req=99999

            # Add entry (if valid)
            if Player["quest"] >= req:
                if Player["ap"] >= cost:
                    areamap.append(("%s (%d AP)" % (name, cost), qid))
                else:
                    areamap.append(("{s}%s (%d AP){/s}" % (name, cost), None))

        # Display area menu
        areamap.append(("Return", -1))
        qid=renpy.display_menu(areamap)
        del areamap
        if qid == -1 or qid is None:
            renpy.jump("quest_select")

    jump quest_selected

###################################################################
label quest_selected:
    # Get quest data
    python:
        quest=dl_search(allquests["Main"], "quest_id", qid)

        # Uhm, how did this happen? Means client-data is not fully updated!
        if (quest == ERR_INVALID):
            renpy.call_screen("msgbox", "ERROR:\n\nRequested Quest does not exist client-side\nAn update is required. We'll now restart the app.")
            renpy.quit(relaunch=True)

    # Confirm the quest cost
    $apmsg=_("Quest cost: %d/%d AP" % (quest["cost"], Player["ap"]))
    menu:
        "[apmsg]"
        "Accept Quest" if Player["ap"] >= quest["cost"]:
            pass
        "Decline Quest":
            jump quest_select

    # Begin the quest
    $ Battle=loadquest(qid)
    $ sdelay(0.02)

    # Check for error
    if (Battle in [FAILUREMSG, OFFLINEMSG, ERR_JSONDECODER, ERR_LOGIN_DEFAULT]):
        $ renpy.call_screen("msgbox", "Error:\n\n%s\nYou'll be taken to town." % Battle)
        jump restore

    # Reduce the paid AP
    python:

        # Consume the AP
        update_ap()

        # Before fighting, should we perhaps show story?
        if Player["quest"] < qid:
            story=dl_search(allstory, "quest_id", qid)

            if (story != ERR_INVALID):
                hud_story()
                print(".:: Story logs (%d) ::." % qid)

                if isinstance(story["pre_dialog"], str) or isinstance(story["pre_dialog"], unicode):
                    print("Calling in new context: %s" % (story["pre_dialog"]))
                    renpy.call_in_new_context(story["pre_dialog"])
                else:
                    bg_is_showing=False
                    for dial in story["pre_dialog"]:
                        # Background
                        if str(dial["bg"]) != "":
                            if bg_is_showing:
                                renpy.hide("sbg")
                            show_img("bg "+dial["bg"], tag="sbg")
                            bg_is_showing=True

                        show_img("dialog_"+dial["left_sprite"], at_list=[tleft], tag="l")
                        show_img("dialog_"+dial["center_sprite"], at_list=[tcenter], tag="c")
                        show_img("dialog_"+dial["right_sprite"], at_list=[tright], tag="r")
                        renpy.say(dial["name"], dial["message"])
                        renpy.hide("l")
                        renpy.hide("c")
                        renpy.hide("r")
                        print("%s: %s" % (dial["name"], dial["message"]))
                    # Background Clean up
                    if bg_is_showing:
                        renpy.hide("sbg")
                    del bg_is_showing

    # Okay, story-telling time is over: To arms!
    # Prepare music/background or use defaults
    python:
        TMP_MUSIC = MUSIC_BATTLE.id()
        TMP_BACKG = "battle"
        try:
            TMP_MUSIC=get_sfx(quest["music"])
            TMP_BACKG=quest["bg"]
        except:
            traceback.print_exc()
    ## Play the music, free memory, to arms!
    #$ print("Stage music: "+str(TMP_MUSIC))
    play music TMP_MUSIC fadein 0.5
    #play music MUSIC_BATTLE.id() fadein 0.5
    $ renpy.free_memory()
    window hide
    $ renpy.stop_predict_screen("battle")
    #$ renpy.jump_out_of_context("combat")
    #$ renpy.call_in_new_context("combat")
    #jump restore
    jump combat


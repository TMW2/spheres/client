################################################################################
## Initialization
################################################################################

init offset = -1


################################################################################
## Styles
################################################################################

style default:
    properties gui.text_properties()
    language gui.language

style input:
    properties gui.text_properties("input", accent=True)
    adjust_spacing False

style hyperlink_text:
    properties gui.text_properties("hyperlink", accent=True)
    hover_underline True

style gui_text:
    properties gui.text_properties("interface")

style button:
    properties gui.button_properties("button")

style button_text is gui_text:
    properties gui.text_properties("button")
    yalign 0.5
    size 30

style label_text is gui_text:
    properties gui.text_properties("label", accent=True)

style prompt_text is gui_text:
    properties gui.text_properties("prompt")


style bar:
    ysize gui.bar_size
    left_bar Frame("gui/bar/left.png", gui.bar_borders, tile=gui.bar_tile)
    right_bar Frame("gui/bar/right.png", gui.bar_borders, tile=gui.bar_tile)

style vbar:
    xsize gui.bar_size
    top_bar Frame("gui/bar/top.png", gui.vbar_borders, tile=gui.bar_tile)
    bottom_bar Frame("gui/bar/bottom.png", gui.vbar_borders, tile=gui.bar_tile)

style scrollbar:
    ysize gui.scrollbar_size
    base_bar Frame("gui/scrollbar/horizontal_[prefix_]bar.png", gui.scrollbar_borders, tile=gui.scrollbar_tile)
    thumb Frame("gui/scrollbar/horizontal_[prefix_]thumb.png", gui.scrollbar_borders, tile=gui.scrollbar_tile)

style vscrollbar:
    xsize gui.scrollbar_size
    base_bar Frame("gui/scrollbar/vertical_[prefix_]bar.png", gui.vscrollbar_borders, tile=gui.scrollbar_tile)
    thumb Frame("gui/scrollbar/vertical_[prefix_]thumb.png", gui.vscrollbar_borders, tile=gui.scrollbar_tile)

style slider:
    ysize gui.slider_size
    base_bar Frame("gui/slider/horizontal_[prefix_]bar.png", gui.slider_borders, tile=gui.slider_tile)
    thumb "gui/slider/horizontal_[prefix_]thumb.png"

style vslider:
    xsize gui.slider_size
    base_bar Frame("gui/slider/vertical_[prefix_]bar.png", gui.vslider_borders, tile=gui.slider_tile)
    thumb "gui/slider/vertical_[prefix_]thumb.png"


style frame:
    padding gui.frame_borders.padding
    background Frame("gui/frame.png", gui.frame_borders, tile=gui.frame_tile)



################################################################################
## In-game screens
################################################################################


## Say screen ##################################################################
##
## The say screen is used to display dialogue to the player. It takes two
## parameters, who and what, which are the name of the speaking character and
## the text to be displayed, respectively. (The who parameter can be None if no
## name is given.)
##
## This screen must create a text displayable with id "what", as Ren'Py uses
## this to manage text display. It can also create displayables with id "who"
## and id "window" to apply style properties.
##
## https://www.renpy.org/doc/html/screen_special.html#say

screen say(who, what):
    style_prefix "say"

    window:
        id "window"

        if who is not None:

            window:
                id "namebox"
                style "namebox"
                text who id "who"

        text what id "what"


    ## If there's a side image, display it above the text. Do not display on the
    ## phone variant - there's no room.
    if not renpy.variant("small"):
        add SideImage() xalign 0.0 yalign 1.0


## Make the namebox available for styling through the Character object.
init python:
    config.character_id_prefixes.append('namebox')

style window is default
style say_label is default
style say_dialogue is default
style say_thought is say_dialogue

style namebox is default
style namebox_label is say_label


style window:
    xalign 0.5
    xfill True
    yfill True
    yalign gui.textbox_yalign
    ysize gui.textbox_height

    background Frame(Image("gui/textbox.png", xalign=0.5, yalign=1.0), 0, 0)

style namebox:
    xpos gui.name_xpos
    xanchor gui.name_xalign
    xsize gui.namebox_width
    ypos gui.name_ypos
    ysize gui.namebox_height

    background Frame("gui/namebox.png", gui.namebox_borders, tile=gui.namebox_tile, xalign=gui.name_xalign)
    padding gui.namebox_borders.padding

style say_label:
    properties gui.text_properties("name", accent=True)
    xalign gui.name_xalign
    yalign 0.5

style say_dialogue:
    properties gui.text_properties("dialogue")

    xpos gui.dialogue_xpos
    xsize gui.dialogue_width
    ypos gui.dialogue_ypos


## Input screen ################################################################
##
## This screen is used to display renpy.input. The prompt parameter is used to
## pass a text prompt in.
##
## This screen must create an input displayable with id "input" to accept the
## various input parameters.
##
## https://www.renpy.org/doc/html/screen_special.html#input

screen input(prompt):
    style_prefix "input"

    window:

        vbox:
            xalign gui.dialogue_text_xalign
            xpos gui.dialogue_xpos
            xsize gui.dialogue_width
            ypos gui.dialogue_ypos

            text prompt style "input_prompt"
            input id "input"

style input_prompt is default

style input_prompt:
    xalign gui.dialogue_text_xalign
    properties gui.text_properties("input_prompt")

style input:
    xalign gui.dialogue_text_xalign
    xmaximum gui.dialogue_width


## Choice screen ###############################################################
##
## This screen is used to display the in-game choices presented by the menu
## statement. The one parameter, items, is a list of objects, each with caption
## and action fields.
##
## https://www.renpy.org/doc/html/screen_special.html#choice

screen choice(items):
    style_prefix "choice"

    vbox:
        for i in items:
            textbutton i.caption:
                xalign 0.5
                xmaximum 0.95
                action i.action


## When this is true, menu captions will be spoken by the narrator. When false,
## menu captions will be displayed as empty buttons.
define config.narrator_menu = True


style choice_vbox is vbox
style choice_button is button
style choice_button_text is button_text

style choice_vbox:
    xalign 0.5
    ypos 0.25
    yanchor 0.0

    spacing gui.choice_spacing

style choice_button is default:
    properties gui.button_properties("choice_button")
    background Frame("gui/WideBB_insensitive.png", 5, 5)

style choice_button_text is default:
    xmaximum 0.92
    ymargin 12
    properties gui.button_text_properties("choice_button")
    size 42


## Quick Menu screen ###########################################################
##
## The quick menu is displayed in-game to provide easy access to the out-of-game
## menus.

screen quick_menu():

    ## Ensure this appears on top of other screens.
    zorder 100

    ## Main HUD Control
    if hud_interface:
        frame:
            #background "#0009"
            background "gui/HUD.png"
            xalign 0.5
            yalign 0.0

            frame:
                style_prefix "quick"
                background "#0000"
                xmargin 50
                ymargin 50
                vbox:
                    spacing 12
                    label _("%s (Lv. %d)" % (persistent.nickname or "Guest", Player["level"])):
                        xalign 0.5
                    if Player["ap"] < Player["max_ap"]:
                        add DynamicDisplayable(countdown, length=Player["aptime"]-now()+360.0):
                            xpos 0.5
                            xanchor 1.0
                    else:
                        add DynamicDisplayable(countdown, length=0.0):
                            xpos 0.5
                            xanchor 1.0
                    hbox:
                        spacing 24
                        label _("XP")
                        bar:
                            value StaticValue(Player["exp"], Player["max_exp"])
                            xsize 140
                            left_bar Color("#6F6")
                            #right_bar
                        label _("%d/%d" % (Player["exp"], Player["max_exp"])):
                            text_size 24
                        text _("$ %d" % Player["gp"]):
                            size 36
                            xalign 1.0
                    hbox:
                        spacing 24
                        label _("AP")
                        bar:
                            value StaticValue(Player["ap"], Player["max_ap"])
                            xsize 140
                        label _("%d/%d" % (Player["ap"], Player["max_ap"])):
                            text_size 24
                        text _("{image=gfx/gui/crystal.png} %d" % Player["crystals"]):
                            size 36
                            xalign 1.0


    ## DownLoader Screen
    showif tr_loading:
        frame:
            background "#0000"
            xalign 0.5
            yalign 0.5
            add "spinner"


    ## Story QuickMenu control
    if story_mode:
        hbox:
            style_prefix "quick"

            xalign 0.5
            yalign 1.0

            textbutton _("History") action ShowMenu('history')
            textbutton _("Skip") action Skip() alternate Skip(fast=True, confirm=True)
            textbutton _("Auto") action Preference("auto-forward", "toggle")
            textbutton _("Prefs") action ShowMenu('preferences')


## This code ensures that the quick_menu screen is displayed in-game, whenever
## the player has not explicitly hidden the interface.
init python:
    config.overlay_screens.append("quick_menu")

    def hud_clear():
        global story_mode, hud_interface
        _window_hide(None)
        story_mode=False
        hud_interface=False

    def hud_show():
        global story_mode, hud_interface
        _window_hide(None)
        story_mode=False
        hud_interface=True

    def hud_story():
        global story_mode, hud_interface
        _window_show(None)
        story_mode=True
        hud_interface=False

#default quick_menu = True
default tr_loading = False
default story_mode = False
default battle_mode = False
default hud_interface = False

style quick_button is default
style quick_button_text is button_text

style quick_button:
    properties gui.button_properties("quick_button")

style quick_button_text:
    properties gui.button_text_properties("quick_button")


################################################################################
## Main and Game Menu Screens
################################################################################

## Navigation screen ###########################################################
##
## This screen is included in the main and game menus, and provides navigation
## to other menus, and to start the game.

screen navigation():

    hbox:
        style_prefix "navigation"

        xpos gui.navigation_xpos
        yalign 0.0

        spacing gui.navigation_spacing

        if main_menu:
            textbutton _("Start") action Start()
        elif story_mode or hud_interface:
            textbutton _("History") action ShowMenu("history")

        textbutton _("Prefs") action ShowMenu("preferences")

        if _in_replay:
            textbutton _("End Replay") action EndReplay(confirm=True)

        elif not main_menu and (debug or config.developer):
            textbutton _("Main Menu") action SpheresMainMenu()

        if main_menu or story_mode or hud_interface:
            textbutton _("About") action ShowMenu("about")

        textbutton _("Help") action ShowMenu("help")

        if battle_mode: # FIXME
            if Player["quest"] >= 5:
                textbutton _("Summons") action ShowMenu("sumconf")

        if not renpy.ios and not renpy.emscripten:

            ## The quit button is banned on iOS and unnecessary on Android and
            ## Web. So renpy.variant("pc") would make sense
            ## renpy.variant() → large/medium/small, tablet/phone, touch, tv,
            ## ouya, firetv, android, ios, mobile, pc, web, etc.
            textbutton _("Quit") action Quit(confirm=not main_menu)


style navigation_button is gui_button
style navigation_button_text is gui_button_text

style navigation_button:
    size_group "navigation"
    properties gui.button_properties("navigation_button")

style navigation_button_text:
    properties gui.button_text_properties("navigation_button")


## Main Menu screen ############################################################
##
## Used to display the main menu when Ren'Py starts.
##
## https://www.renpy.org/doc/html/screen_special.html#main-menu

screen main_menu():

    ## This ensures that any other menu screen is replaced.
    tag menu

    ## Creates main background
    fixed:
        frame:
            xfill True
            yfill True
            background Frame("gfx/bg/forest_sunset.png", 0, 0)
        image "gfx/nourishedflower.webp":
            xalign 0.5
            yalign 1.0
        frame:
            xfill True
            yfill True
            background Frame("gfx/bg/forest_sunset_fg.png", 0, 0)

    ## Start button
    vbox:
        xalign 0.5
        yalign 0.88
        xfill False
        yfill False
        textbutton _("{size=52}{color=#003}{font=f/ShadowedBlack.ttf}Start{/font}{/color}{/size}"):
            #hover_color "#F00"
            xpadding 42
            ypadding 42
            action Start()
            background Frame("gui/elegant.png", 0, 0)

    ## Settings button
    hbox:
        xalign 0.88
        yalign 0.06
        xfill False
        yfill False
        textbutton _("{color=#FFC}Preferences{/color}"):
            xpadding 20
            ypadding 20
            action ShowMenu("preferences")
            background Frame("gui/frame.png", 5, 5)

    ## Name & Version
    if gui.show_name:
        vbox:
            xalign 0.96
            yalign 0.96
            text "[config.name!t]":
                style "main_menu_title"

            text "[config.version]":
                style "main_menu_version"


style main_menu_frame is empty
style main_menu_vbox is vbox
style main_menu_text is gui_text
style main_menu_title is main_menu_text
style main_menu_version is main_menu_text

style main_menu_frame:
    xfill True
    yfill True

    background Frame("gui/overlay/main_menu.png", 0, 0)

style main_menu_vbox:
    xalign 1.0
    xoffset -20
    xmaximum 800
    yalign 1.0
    yoffset -20

style main_menu_text:
    properties gui.text_properties("main_menu", accent=True)

style main_menu_title:
    properties gui.text_properties("title")

style main_menu_version:
    properties gui.text_properties("version")


## Game Menu screen ############################################################
##
## This lays out the basic common structure of a game menu screen. It's called
## with the screen title, and displays the background, title, and navigation.
##
## The scroll parameter can be None, or one of "viewport" or "vpgrid". When
## this screen is intended to be used with one or more children, which are
## transcluded (placed) inside it.

screen game_menu(title, scroll=None, yinitial=0.0):

    style_prefix "game_menu"

    if main_menu:
        add gui.main_menu_background
    else:
        add gui.game_menu_background

    frame:
        style "game_menu_outer_frame"

        vbox:
            null height 12
            use navigation
            null height 44
            textbutton _("Return"):
                style "return_button"

                action Return()
            null height 20
            hbox:
                frame:
                    style "game_menu_content_frame"
                    if scroll == "viewport":
                        viewport:
                            yinitial yinitial
                            scrollbars "vertical"
                            mousewheel True
                            draggable True
                            pagekeys True
                            side_yfill True
                            vbox:
                                transclude
                    elif scroll == "vpgrid":
                        vpgrid:
                            cols 1
                            yinitial yinitial
                            scrollbars "vertical"
                            mousewheel True
                            draggable True
                            pagekeys True
                            side_yfill True
                            transclude
                    else:
                        transclude

    label title

    if main_menu:
        key "game_menu" action ShowMenu("main_menu")


style game_menu_outer_frame is empty
style game_menu_navigation_frame is empty
style game_menu_content_frame is empty
style game_menu_viewport is gui_viewport
style game_menu_side is gui_side
style game_menu_scrollbar is gui_vscrollbar

style game_menu_label is gui_label
style game_menu_label_text is gui_label_text

style return_button is navigation_button
style return_button_text is navigation_button_text

style game_menu_outer_frame:
    bottom_padding 30
    top_padding 120

    background Frame("gui/overlay/confirm.png", 0, 0)
    #background Frame("gui/overlay/game_menu.png", 0, 0)

style game_menu_navigation_frame:
    xsize 280
    yfill True

style game_menu_content_frame:
    left_margin 40
    right_margin 20
    top_margin 10

style game_menu_viewport:
    xsize 920

style game_menu_vscrollbar:
    unscrollable gui.unscrollable

style game_menu_side:
    spacing 10

style game_menu_label:
    xpos 50
    ysize 120

style game_menu_label_text:
    size gui.title_text_size
    color gui.accent_color
    yalign 0.5

style return_button:
    xpos gui.navigation_xpos
    yalign 1.0
    yoffset -30


## About screen ################################################################
##
## This screen gives credit and copyright information about the game and Ren'Py.
##
## There's nothing special about this screen, and hence it also serves as an
## example of how to make a custom screen.

screen about():

    tag menu

    ## This use statement includes the game_menu screen inside this one. The
    ## vbox child is then included inside the viewport inside the game_menu
    ## screen.
    use game_menu(_("About"), scroll="viewport"):

        style_prefix "about"

        vbox:

            label "[config.name!t]"
            text _("Version [config.version!t] [persistent.release_name!t]\n")

            ## gui.about is usually set in options.rpy.
            if gui.about:
                text "[gui.about!t]\n"

            text _("Made with {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]")

            ## TODO: Obfuscate (e.g. click here to show password)
            if persistent.password:
                null height 60
                text "-------------------------------------"
                label _("Recovery password")
                null height 10
                if len(persistent.password) == 16:
                    text "%s %s %s %s" % (persistent.password[0:4], persistent.password[4:8], persistent.password[8:12], persistent.password[12:16])
                elif len(persistent.password) == 12:
                    text "%s %s %s" % (persistent.password[0:4], persistent.password[4:8], persistent.password[8:12])
                else:
                    text "%s" % persistent.password
                null height 30
                text _("This password allows anyone to login as you.\nKeep it safe!")

## This is redefined in options.rpy to add text to the about screen.
define gui.about = ""


style about_label is gui_label
style about_label_text is gui_label_text
style about_text is gui_text

style about_label_text:
    size gui.label_text_size


## Load and Save screens #######################################################
##
## These screens are responsible for letting the player save the game and load
## it again. Since they share nearly everything in common, both are implemented
## in terms of a third screen, file_slots.
##
## https://www.renpy.org/doc/html/screen_special.html#save https://
## www.renpy.org/doc/html/screen_special.html#load

screen save():

    tag menu


screen load():

    tag menu

style page_label is gui_label
style page_label_text is gui_label_text
style page_button is gui_button
style page_button_text is gui_button_text

style slot_button is gui_button
style slot_button_text is gui_button_text
style slot_time_text is slot_button_text
style slot_name_text is slot_button_text

style page_label:
    xpadding 50
    ypadding 3

style page_label_text:
    text_align 0.5
    layout "subtitle"
    hover_color gui.hover_color

style page_button:
    properties gui.button_properties("page_button")

style page_button_text:
    properties gui.button_text_properties("page_button")

style slot_button:
    properties gui.button_properties("slot_button")

style slot_button_text:
    properties gui.button_text_properties("slot_button")


## About screen ################################################################
##
## This screen gives credit and copyright information about the game and Ren'Py.
##
## There's nothing special about this screen, and hence it also serves as an
## example of how to make a custom screen.

screen sumconf():

    tag menu

    ## This use statement includes the game_menu screen inside this one. The
    ## vbox child is then included inside the viewport inside the game_menu
    ## screen.
    use game_menu(_("Configure Summon"), scroll="viewport"):

        style_prefix "about"

        vbox:
            for i, e in enumerate(allsummons):
                if i < Player["max_sum"] and Player["quest"] >= 5:
                    hbox:
                        if persistent.summon == i:
                            image "gui/up_on.png"
                        else:
                            button:
                                action SetVariable("persistent.summon", i)
                                add "gui/up_off.png"
                        label _(" %s" % e["name"])
                    text _("%s" % e["desc"])
                null height 10

            if Player["quest"] < 5:
                label _("You still don't know how to summon.")
                null height 24
                text _("Maybe once you reach the stronghold, someone will teach you?")
            else:
                text _("\n[config.name!t] [config.version!t] [persistent.release_name!t]")


## Preferences screen ##########################################################
##
## The preferences screen allows the player to configure the game to better suit
## themselves.
##
## https://www.renpy.org/doc/html/screen_special.html#preferences

screen preferences():

    tag menu

    use game_menu(_("Preferences"), scroll="viewport"):

        vbox:

            hbox:
                box_wrap True

                if renpy.variant("pc") or renpy.variant("web"):

                    vbox:
                        style_prefix "radio"
                        label _("Display")
                        textbutton _("Window") action Preference("display", "window")
                        textbutton _("Fullscreen") action Preference("display", "fullscreen")

                vbox:
                    style_prefix "check"
                    label _("Skip")
                    textbutton _("Unseen Text") action Preference("skip", "toggle")
                    textbutton _("After Choices") action Preference("after choices", "toggle")
                    textbutton _("Transitions") action InvertSelected(Preference("transitions", "toggle"))

                ## Additional vboxes of type "radio_pref" or "check_pref" can be
                ## added here, to add additional creator-defined preferences.

            null height (4 * gui.pref_spacing)

            hbox:
                style_prefix "slider"
                box_wrap True

                vbox:

                    label _("Text Speed")

                    bar value Preference("text speed")

                    label _("Auto-Forward Time")

                    bar value Preference("auto-forward time")

                    if main_menu:
                        null height 60
                        textbutton _("Clear Cache"):
                            action Jump("clear_cache")
                        null height 30
                        textbutton _("Download All Files"):
                            action Jump("download_all")

                vbox:

                    if config.has_music:
                        label _("Music Volume")

                        hbox:
                            bar value Preference("music volume")

                    if config.has_sound:

                        label _("Sound Volume")

                        hbox:
                            bar value Preference("sound volume")

                            if config.sample_sound:
                                textbutton _("Test") action Play("sound", config.sample_sound)


                    if config.has_voice:
                        label _("Voice Volume")

                        hbox:
                            bar value Preference("voice volume")

                            if config.sample_voice:
                                textbutton _("Test") action Play("voice", config.sample_voice)

                    if config.has_music or config.has_sound or config.has_voice:
                        null height gui.pref_spacing

                        textbutton _("Mute All"):
                            action Preference("all mute", "toggle")
                            style "mute_all_button"

                if main_menu:
                    hbox:
                        vbox:
                            style_prefix "check"
                            null height 60
                            label _("Server")
                            for k in persistent.serverlist:
                                # FIXME
                                textbutton _(k[0]):
                                    action [
                                            SetVariable("persistent.host", k[1]),
                                            SetVariable("persistent.port", k[2]),
                                            Jump("clear_all")]
                            showif debug:
                                null height 30
                                textbutton _("+ New Server"):
                                    action Call("add_server")
                        vbox:
                            style_prefix "check"
                            null height 60
                            label _("Debug Mode")
                            textbutton _("Enabled"):
                                action SetVariable("debug", True)
                            textbutton _("Disabled"):
                                action SetVariable("debug", False)

                showif config.developer or debug:
                    vbox:
                        style_prefix "check"
                        null height 60
                        label _("SSL:")
                        if not config.developer:
                            null height 10
                            label _("{size=30}{color=#f00}WARNING! This is an Advanced option.\nDO NOT change it unless you know what you are doing!{/color}{/size}")
                            null height 15
                        textbutton _("Enable"):
                            action SetVariable("persistent.ssl_enabled", True)
                        textbutton _("Disable"):
                            action SetVariable("persistent.ssl_enabled", False)
                        textbutton _("Ignore"):
                            action SetVariable("persistent.ssl_enabled", "IGNORE")

                showif config.developer or debug or persistent.irc_enable:
                    vbox:
                        style_prefix "check"
                        null height 60
                        label _("IRC:")
                        if not config.developer:
                            null height 10
                            label _("{size=30}{color=#f00}WARNING! This is an Advanced option.\nDO NOT change it unless you know what you are doing!{/color}{/size}")
                            null height 15
                        textbutton _("Enable"):
                            action SetVariable("persistent.irc_enable", True)
                        textbutton _("Disable"):
                            action SetVariable("persistent.irc_enable", False)

style pref_label is gui_label
style pref_label_text is gui_label_text
style pref_vbox is vbox

style radio_label is pref_label
style radio_label_text is pref_label_text
style radio_button is gui_button
style radio_button_text is gui_button_text
style radio_vbox is pref_vbox

style check_label is pref_label
style check_label_text is pref_label_text
style check_button is gui_button
style check_button_text is gui_button_text
style check_vbox is pref_vbox

style slider_label is pref_label
style slider_label_text is pref_label_text
style slider_slider is gui_slider
style slider_button is gui_button
style slider_button_text is gui_button_text
style slider_pref_vbox is pref_vbox

style mute_all_button is check_button
style mute_all_button_text is check_button_text

style check_button_text:
    size 32

style navigation_button_text:
    size 32

style pref_label:
    top_margin gui.pref_spacing
    bottom_margin 2

style pref_label_text:
    yalign 1.0

style pref_vbox:
    xsize 225

style radio_vbox:
    spacing gui.pref_button_spacing

style radio_button:
    properties gui.button_properties("radio_button")
    foreground "gui/button/radio_[prefix_]foreground.png"

style radio_button_text:
    properties gui.button_text_properties("radio_button")

style check_vbox:
    spacing gui.pref_button_spacing

style check_button:
    properties gui.button_properties("check_button")
    foreground "gui/button/check_[prefix_]foreground.png"

style check_button_text:
    properties gui.button_text_properties("check_button")

style slider_slider:
    xsize 350

style slider_button:
    properties gui.button_properties("slider_button")
    yalign 0.5
    left_margin 10

style slider_button_text:
    properties gui.button_text_properties("slider_button")

style slider_vbox:
    xsize 450


## History screen ##############################################################
##
## This is a screen that displays the dialogue history to the player. While
## there isn't anything special about this screen, it does have to access the
## dialogue history stored in _history_list.
##
## https://www.renpy.org/doc/html/history.html

screen history():

    tag menu

    ## Avoid predicting this screen, as it can be very large.
    predict False

    use game_menu(_("History"), scroll=("vpgrid" if gui.history_height else "viewport"), yinitial=1.0):

        style_prefix "history"

        for h in _history_list:

            window:

                ## This lays things out properly if history_height is None.
                has fixed:
                    yfit True

                if h.who:

                    label h.who:
                        style "history_name"
                        substitute False

                        ## Take the color of the who text from the Character, if
                        ## set.
                        if "color" in h.who_args:
                            text_color h.who_args["color"]

                $ what = renpy.filter_text_tags(h.what, allow=gui.history_allow_tags)
                text what:
                    substitute False

        if not _history_list:
            label _("The dialogue history is empty.")


## This determines what tags are allowed to be displayed on the history screen.

define gui.history_allow_tags = set()


style history_window is empty

style history_name is gui_label
style history_name_text is gui_label_text
style history_text is gui_text

style history_text is gui_text

style history_label is gui_label
style history_label_text is gui_label_text

style history_window:
    xfill True
    ysize gui.history_height

style history_name:
    xpos gui.history_name_xpos
    xanchor gui.history_name_xalign
    ypos gui.history_name_ypos
    xsize gui.history_name_width

style history_name_text:
    min_width gui.history_name_width
    text_align gui.history_name_xalign

style history_text:
    xpos gui.history_text_xpos
    ypos gui.history_text_ypos
    xanchor gui.history_text_xalign
    xsize gui.history_text_width
    min_width gui.history_text_width
    text_align gui.history_text_xalign
    layout ("subtitle" if gui.history_text_xalign else "tex")

style history_label:
    xfill True

style history_label_text:
    xalign 0.5



################################################################################
## Additional screens
################################################################################


## Confirm screen ##############################################################
##
## The confirm screen is called when Ren'Py wants to ask the player a yes or no
## question.
##
## https://www.renpy.org/doc/html/screen_special.html#confirm

screen confirm(message, yes_action, no_action):

    ## Ensure other screens do not get input while this screen is displayed.
    modal True

    zorder 200

    style_prefix "confirm"

    add "gui/overlay/confirm.png"

    frame:

        vbox:
            xalign .5
            yalign .5
            spacing 30

            label _(message):
                style "confirm_prompt"
                xalign 0.5

            hbox:
                xalign 0.5
                spacing 100

                textbutton _("Yes") action yes_action
                textbutton _("No") action no_action

    ## Right-click and escape answer "no".
    key "game_menu" action no_action

transform msgbox_emp:
        xalign 0.51
        yalign 0.51
        linear 0.1
        xalign 0.49
        yalign 0.49
        linear 0.1
        xalign 0.51
        yalign 0.51
        linear 0.1
        xalign 0.49
        yalign 0.49
        linear 0.1
        xalign 0.50
        yalign 0.50
        linear 0.1

screen msgbox(message, anim=True, autoclose=0.0):
    ## Ensure other screens do not get input while this screen is displayed.
    modal True

    zorder 200

    style_prefix "confirm"

    frame:
        if anim:
            at msgbox_emp

        vbox:
            xalign 0.5
            yalign 0.5
            spacing 30

            label _(message):
                style "confirm_prompt"
                xalign 0.5

            hbox:
                xalign 0.5
                spacing 100

                textbutton _("Ok") action Return()

    if autoclose > 0.0:
        timer autoclose action Return()

    ## Right-click and escape answer "no".
    key "game_menu" action Return()


screen show_news():
    default current=0
    ## Ensure other screens do not get input while this screen is displayed.
    modal True

    zorder 200

    frame:
        background Frame("gui/frame.png", 0, 0)
        xalign 0.5
        yalign 0.5
        ymargin 15
        ypadding 10
        xmargin 10

        hbox:
            spacing 30
            xmaximum 0.85
            ymaximum 0.85

            # Navigation
            viewport:
                child_size (0.4, 0.8)
                mousewheel True
                draggable True
                pagekeys True
                xfill False

                vbox:
                    spacing 2
                    label _("Game news")
                    $i=0
                    for entry in allnews:
                        textbutton "{size=18}"+_(entry["title"])+"{/size}" action SetScreenVariable("current", i)
                        if not i:
                            null height 5
                            label "{size=18}"+_("Older entries:")+"{/size}"
                            null height 5
                        $i+=1
                    $del i

            # News screen
            viewport:
                #child_size (0.4, 0.8)
                mousewheel True
                draggable True
                arrowkeys True
                xfill False
                scrollbars "vertical"

                vbox:
                    xalign 0.5
                    spacing 20
                    label _(allnews[current]["title"])
                    label "{size=22}"+_(allnews[current]["date"])+"{/size}"

                    null height 40
                    label "{size=20}"+_(allnews[current]["body"])+"{/size}"

                    null height 20
                    textbutton _("Ok") action Return()

    ## Right-click and escape answer "no".
    key "game_menu" action Return()


style confirm_frame is gui_frame
style confirm_prompt is gui_prompt
style confirm_prompt_text is gui_prompt_text
style confirm_button is gui_medium_button
style confirm_button_text is gui_medium_button_text

style confirm_frame:
    background Frame([ "gui/confirm_frame.png", "gui/frame.png"], gui.confirm_frame_borders, tile=gui.frame_tile)
    padding gui.confirm_frame_borders.padding
    xalign .5
    yalign .5

style confirm_prompt_text:
    text_align 0.5
    layout "subtitle"

style confirm_button:
    properties gui.button_properties("confirm_button")

style confirm_button_text:
    properties gui.button_text_properties("confirm_button")


## Skip indicator screen #######################################################
##
## The skip_indicator screen is displayed to indicate that skipping is in
## progress.
##
## https://www.renpy.org/doc/html/screen_special.html#skip-indicator

screen skip_indicator():

    zorder 100
    style_prefix "skip"

    frame:

        hbox:
            spacing 6

            text _("Skipping")

            text "▸" at delayed_blink(0.0, 1.0) style "skip_triangle"
            text "▸" at delayed_blink(0.2, 1.0) style "skip_triangle"
            text "▸" at delayed_blink(0.4, 1.0) style "skip_triangle"


## This transform is used to blink the arrows one after another.
transform delayed_blink(delay, cycle):
    alpha .5

    pause delay

    block:
        linear .2 alpha 1.0
        pause .2
        linear .2 alpha 0.5
        pause (cycle - .4)
        repeat


style skip_frame is empty
style skip_text is gui_text
style skip_triangle is skip_text

style skip_frame:
    ypos gui.skip_ypos
    background Frame("gui/skip.png", gui.skip_frame_borders, tile=gui.frame_tile)
    padding gui.skip_frame_borders.padding

style skip_text:
    size gui.notify_text_size

style skip_triangle:
    ## We have to use a font that has the BLACK RIGHT-POINTING SMALL TRIANGLE
    ## glyph in it.
    font "DejaVuSans.ttf"


## Notify screen ###############################################################
##
## The notify screen is used to show the player a message. (For example, when
## the game is quicksaved or a screenshot has been taken.)
##
## https://www.renpy.org/doc/html/screen_special.html#notify-screen

screen notify(message):

    zorder 100
    style_prefix "notify"

    frame at notify_appear:
        text "[message!tq]"

    timer 3.25 action Hide('notify')


transform notify_appear:
    on show:
        alpha 0
        linear .25 alpha 1.0
    on hide:
        linear .5 alpha 0.0


style notify_frame is empty
style notify_text is gui_text

style notify_frame:
    ypos gui.notify_ypos

    background Frame("gui/notify.png", gui.notify_frame_borders, tile=gui.frame_tile)
    padding gui.notify_frame_borders.padding

style notify_text:
    properties gui.text_properties("notify")


################################################################################
## Mobile Variants
################################################################################

#style pref_vbox:
#    variant "medium"
#    xsize 450

style window:
    variant "small"
    background Frame("gui/phone/textbox.png", 0, 0)

style radio_button:
    variant "small"
    foreground "gui/phone/button/radio_[prefix_]foreground.png"

style check_button:
    variant "small"
    foreground "gui/phone/button/check_[prefix_]foreground.png"

#style nvl_window:
#    variant "small"
#    background Frame("gui/phone/nvl.png", 0, 0)

#style main_menu_frame:
#    variant "small"
#    background "gui/phone/overlay/main_menu.png"

#style game_menu_outer_frame:
#    variant "small"
#    background "gui/phone/overlay/game_menu.png"

#style game_menu_navigation_frame:
#    variant "small"
#    xsize 340

#style game_menu_content_frame:
#    variant "small"
#    top_margin 0

#style pref_vbox:
#    variant "small"
#    xsize 400

style bar:
    variant "small"
    ysize gui.bar_size
    left_bar Frame("gui/phone/bar/left.png", gui.bar_borders, tile=gui.bar_tile)
    right_bar Frame("gui/phone/bar/right.png", gui.bar_borders, tile=gui.bar_tile)

style vbar:
    variant "small"
    xsize gui.bar_size
    top_bar Frame("gui/phone/bar/top.png", gui.vbar_borders, tile=gui.bar_tile)
    bottom_bar Frame("gui/phone/bar/bottom.png", gui.vbar_borders, tile=gui.bar_tile)

style scrollbar:
    variant "small"
    ysize gui.scrollbar_size
    base_bar Frame("gui/phone/scrollbar/horizontal_[prefix_]bar.png", gui.scrollbar_borders, tile=gui.scrollbar_tile)
    thumb Frame("gui/phone/scrollbar/horizontal_[prefix_]thumb.png", gui.scrollbar_borders, tile=gui.scrollbar_tile)

style vscrollbar:
    variant "small"
    xsize gui.scrollbar_size
    base_bar Frame("gui/phone/scrollbar/vertical_[prefix_]bar.png", gui.vscrollbar_borders, tile=gui.scrollbar_tile)
    thumb Frame("gui/phone/scrollbar/vertical_[prefix_]thumb.png", gui.vscrollbar_borders, tile=gui.scrollbar_tile)

style slider:
    variant "small"
    ysize gui.slider_size
    base_bar Frame("gui/phone/slider/horizontal_[prefix_]bar.png", gui.slider_borders, tile=gui.slider_tile)
    thumb "gui/phone/slider/horizontal_[prefix_]thumb.png"

style vslider:
    variant "small"
    xsize gui.slider_size
    base_bar Frame("gui/phone/slider/vertical_[prefix_]bar.png", gui.vslider_borders, tile=gui.slider_tile)
    thumb "gui/phone/slider/vertical_[prefix_]thumb.png"

#style slider_pref_vbox:
#    variant "small"
#    xsize None

style slider_pref_slider:
    variant "small"
    xsize 600

screen input_box(ib_message=_("Please write it down here: "), baloney=""):
    window:
        background Frame("gui/frame.png", 5, 5)
        yalign 0.25
        xalign 0.5
        ypadding 30
        xpadding 30
        xmaximum 512
        xminimum 512
        yminimum 95
        vbox:
            text (ib_message) color "#3ee33e" yalign 0.0 size 28
            input:
                id "msgmsg"
                color ((128, 128, 128, 220))
                italic True
                size 26
                copypaste True
    fixed:
        label (baloney):
            style "pref_label"
            yalign 0.02
            xalign 0.50
            text_size 54
            text_outlines [ (1, "#000", 0, 0) ]



screen notabox(message):

    zorder 250
    #style_prefix "notify"
    style_prefix "confirm"

    frame at notify_appear:
        vbox:
            xalign 0.5
            yalign 0.5
            spacing 30

            label "{b}SERVER NOTICE{/b}":
                xalign 0.5

            label "[message!tq]":
                style "confirm_prompt"
                xalign 0.5

            hbox:
                xalign 0.5
                spacing 100

                textbutton _("Ok") action Hide('notabox')

    timer 60.00 action Hide('notabox')

init python:
    def display_msgbox(message):
        """
        :doc: other

        Override implementation of :func:`renpy.notify`.
        """

        renpy.hide_screen('notabox')
        renpy.show_screen('notabox', message=message)
        renpy.restart_interaction()



init python:
    # countdown for AP Timer
    def countdown(st, at, length=0.0):

        remaining = length - st
        mins = remaining/60
        secs = remaining%60

        if remaining > 0.0:
            return Text("%02d:%02d" % (mins, secs), color="#fff", size=24), .1
        else:
            return Text("00:00", color="#fff", size=24), None

#image countdown = DynamicDisplayable(countdown, length=120.0)


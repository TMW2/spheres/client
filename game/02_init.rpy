########################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Definitions: decode, encode, JSON parser and memory
############################################################################
init -1 python:
    stdout("======================= %s %s %s" % (config.name, config.version, persistent.release_name))
    print("[STDBY] Loading Basic functions.......")

    # Search for array[?][key]==search in an array of dicts
    # Returns the dictionary, or returns ERR_INVALID
    def dl_search(array, key, search):
        try:
            if (PYTHON_VERSION < 3000):
                r=(item for item in array if item[key] == search).next()
            else:
                r=next(item for item in array if item[key] == search)
        except:
            r=ERR_INVALID
        if r is None:
            r=ERR_INVALID
            stdout("dlsearch: r is None")
        return r

    def check_fail(raw):
        global debug, FAILUREMSG

        if (debug):
            stdout(str(raw))
        if (raw == FAILUREMSG):
            return True
        return False

    def json_decode(raw):
        global ERRNO
        #stdout("Decoder received %s" % repr(raw))

        if (check_fail(raw)):
            return ERR_LOGIN_DEFAULT

        # TODO Move this to check_fail and rewrite check_fail
        # ERR_OFF should be handled top-level no? With full_restart()
        if raw in ERRNO:
            return raw

        if (type(raw) is int):
            return int(raw)

        # Maybe base 64
        try:
            rw=base64.b64decode(raw)
            raw=rw
            if (debug):
                print("base64 decoded")
        except:
            traceback.print_exc()
            pass

        # Maybe zlib compressed
        try:
            rw=zlib.decompress(raw)
            raw=rw
            if (debug):
                print(str(raw))
        except:
            traceback.print_exc()
            pass

        # Decode JSON
        try:
            return json.loads(raw)
        except:
            traceback.print_exc()
            return ERR_JSONDECODER

    def get_token():
        try:
            t=Player['token']
        except:
            t="0" #"f528764d624db129b32c21fbca0cb8d6"
        return t

    def login():
        global Player
        stdout("Login requested for %s" % logindata())
        raw=send_packet("login", logindata())

        Player=json_decode(raw)
        if (Player == ERR_JSONDECODER):
            return ERR_JSONDECODER
        if (Player == ERR_LOGIN_DEFAULT):
            return ERR_LOGIN_DEFAULT
        if (Player == ERR_OUTDATED):
            return ERR_OUTDATED

        try:
            Player["inv"]=dlist()
        except:
            pass
        return Player["code"]

    def GAME_LOADER():
        global allunitsbase, allunits, allquests, allstory, allworld, alltaverns
        global allnews, allsummons, tr_uptodate, tr_memcheck, tr_fatality

        # Wait until everything is up to date
        while not tr_uptodate:
            sdelay()

        try:
            # Load unit data
            #allunitsbase=json.loads(requests.get("http://"+HOST+'/units.json', verify=False).text)
            stdout("Loading units...")
            f=open(get_path_if_exists("units.json"), "r")
            allunitsbase=json.load(f)
            f.close()

            # Reorder unit data
            allunits={}
            for j in allunitsbase:
                allunits[j["unit_id"]]=j

            # Load summons data
            stdout("Loading summons...")
            f=open(get_path_if_exists("summons.json"), "r")
            allsummons=json.load(f)
            f.close()

            # Load quest data
            stdout("Loading quests...")
            f=open(get_path_if_exists("quests.json"), "r")
            allquests=json.load(f)
            f.close()

            # Load story data
            stdout("Loading story...")
            f=open(get_path_if_exists("story.json"), "r")
            allstory=json.load(f)
            f.close()

            # Load world data
            stdout("Loading world...")
            f=open(get_path_if_exists("world.json"), "r")
            allworld=json.load(f)
            f.close()

            # Load tavern data
            stdout("Loading tavern...")
            f=open(get_path_if_exists("bar.json"), "r")
            alltaverns=json.load(f)
            f.close()

            stdout("PREPARING FOR NEWS")
            # Load server news
            try:
                allnews=json.loads(requests.get("http://"+HOST+'/news.json', verify=False, timeout=5.0).text)
            except:
                allnews=[]
                pass

            stdout("[OK] Game data loaded to memory")
        except:
            tr_fatality = True
            traceback.print_exc()
            stdout("[FATAL] Unable to load game data; Aborted.")

        tr_memcheck=True
        return tr_load



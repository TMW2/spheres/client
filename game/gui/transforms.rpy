########################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Transforms for the displayables
init offset = -1

transform enemy1:
    xalign 0.5
    yalign 0.5

transform enemy2:
    xalign 1.0
    yalign 0.5

transform enemy3:
    xalign 0.0
    yalign 0.5

transform party1:
    xpos 0.25
    xanchor 1.0
    yalign 1.0

transform party2:
    xpos 0.50
    xanchor 1.0
    yalign 1.0

transform party3:
    xpos 0.75
    xanchor 1.0
    yalign 1.0

transform party4:
    xpos 1.0
    xanchor 1.0
    yalign 1.0

#transform party5:
#    xalign 1.0
#    yalign 1.0

transform c_party1:
    xpos 0.25
    xanchor 1.0
    zoom 0.4

transform c_party2:
    xpos 0.50
    xanchor 1.0
    zoom 0.4

transform c_party3:
    xpos 0.75
    xanchor 1.0
    zoom 0.4

transform c_party4:
    xpos 1.0
    xanchor 1.0
    zoom 0.4

#transform c_party5:
#    xalign 1.0
#    yalign 1.0
#    zoom 0.25


transform czoom_on:
    yanchor 0.5
    yzoom 1.1
    linear 0.1 yzoom 1.2
    linear 0.2 yzoom 1.1

transform czoom_e1:
    pass
transform czoom_e2:
    pass
transform czoom_e3:
    pass
transform czoom_p1:
    pass
transform czoom_p2:
    pass
transform czoom_p3:
    pass
transform czoom_p4:
    pass
#transform czoom_p5:
#    pass
transform czoom_dd:
    pass

transform czoom_70:
    zoom 0.70

transform czoom_75:
    zoom 0.75


transform prologue_right1:
    xalign 0.67
    yanchor 1.0
    ypos (1824-gui.textbox_height)

transform prologue_right2:
    xanchor 1.0
    xpos 1.5
    yanchor 1.0
    ypos (1824-gui.textbox_height)

transform prologue_right2:
    xanchor 1.0
    xpos 1.25
    yanchor 1.0
    ypos (1824-gui.textbox_height)

transform prologue_center:
    xalign 0.42
    yanchor 1.0
    ypos (1824-gui.textbox_height)

transform prologue_left:
    xanchor 0.0
    xpos -0.32
    yanchor 1.0
    ypos (1824-gui.textbox_height)


transform tcenter:
    xalign 0.5
    yanchor 1.0
    ypos (1824-gui.textbox_height)

transform tleft:
    xalign 0.0
    yanchor 1.0
    ypos (1824-gui.textbox_height)

transform tright:
    xalign 1.0
    yanchor 1.0
    ypos (1824-gui.textbox_height)

transform toffscreenleft:
    xpos 0.0
    xanchor 1.0
    yanchor 1.0
    ypos (1824-gui.textbox_height)

transform toffscreenright:
    xpos 1.0
    xanchor 0.0
    yanchor 1.0
    ypos (1824-gui.textbox_height)

transform ttop:
    xalign 0.5
    yanchor 0.0
    ypos (gui.textbox_height)

transform tcustom(xaxis):
    xalign xaxis
    yanchor 1.0
    ypos (1824-gui.textbox_height)

transform tzoom(val):
    zoom val

# FIXME: Why not use `linear x zoom x` instead of this... ugly... mess?
transform tzoomin:
    zoom 0.1
    pause 0.05
    zoom 0.2
    pause 0.05
    zoom 0.3
    pause 0.05
    zoom 0.4
    pause 0.05
    zoom 0.5
    pause 0.05
    zoom 0.6
    pause 0.05
    zoom 0.7
    pause 0.05
    zoom 0.8
    pause 0.05
    zoom 0.9
    pause 0.05
    zoom 1.0
    pause 0.1
    zoom 1.2
    pause 0.1
    zoom 1.4
    pause 0.15
    zoom 1.2
    pause 0.1
    zoom 1.0


## Help screen #################################################################
##
## A screen that gives information about key and mouse bindings. It uses other
## screens (keyboard_help, mouse_help, and gamepad_help) to display the actual
## help.
##
## It is not used in Mana Spheres mobile client
init offset = -1

screen help():

    tag menu

    default device = "spheres"

    use game_menu(_("Help"), scroll="viewport"):

        style_prefix "help"

        vbox:
            spacing 15

            hbox:

                textbutton _("Spheres") action SetScreenVariable("device", "spheres")

                if renpy.variant("pc") or (renpy.variant("web") and not renpy.variant("mobile")):
                    textbutton _("Keyboard") action SetScreenVariable("device", "keyboard")

                    textbutton _("Mouse") action SetScreenVariable("device", "mouse")

                if GamepadExists():
                    textbutton _("Gamepad") action SetScreenVariable("device", "gamepad")

            if device == "keyboard":
                use keyboard_help
            elif device == "mouse":
                use mouse_help
            elif device == "gamepad":
                use gamepad_help
            elif device == "spheres":
                use spheres_help


screen keyboard_help():

    hbox:
        label _("Enter")
        text _("Advances dialogue and activates the interface.")

    hbox:
        label _("Space")
        text _("Advances dialogue without selecting choices.")

    hbox:
        label _("Arrow Keys")
        text _("Navigate the interface.")

    hbox:
        label _("Ctrl")
        text _("Skips dialogue while held down.")

    hbox:
        label _("Tab")
        text _("Toggles dialogue skipping.")

    hbox:
        label "H"
        text _("Hides the user interface.")

    hbox:
        label "S"
        text _("Takes a screenshot.")

    hbox:
        label "V"
        text _("Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}.")

    null height 64
    hbox:
        label _("Note:")
        text _("Spheres can be manipulated with {color=#0f0}Alt{/color}, followed by the party member number.")
    hbox:
        label _("Example:")
        text _("Alt+1 to select first sphere, Alt+3 to swap with third sphere.")

screen mouse_help():

    hbox:
        label _("Left Click")
        text _("Advances dialogue and activates the interface.")

    hbox:
        label _("Middle Click")
        text _("Hides the user interface.")

    hbox:
        label _("Right Click")
        text _("Accesses the game menu.")


screen gamepad_help():

    hbox:
        label _("Right Trigger\nA/Bottom Button")
        text _("Advances dialogue and activates the interface.")

    hbox:
        label _("D-Pad, Sticks")
        text _("Navigate the interface.")

    hbox:
        label _("Start, Guide")
        text _("Accesses the game menu.")

    hbox:
        label _("Y/Top Button")
        text _("Hides the user interface.")

    textbutton _("Calibrate") action GamepadCalibrate()



screen spheres_help():

    hbox:
        add "gfx/sphere/1.png"
        label _("Attack Row")
        text _("Attacks all enemies.")

    hbox:
        add "gfx/sphere/2.png"
        label _("Pierce")
        text _("Attack with double power.")

    hbox:
        add "gfx/sphere/3.png"
        label _("Shockwave")
        text _("May cause additional damage.")

    hbox:
        add "gfx/sphere/4.png"
        label _("Heal")
        text _("Recover 30%% HP.")

    hbox:
        add "gfx/sphere/5.png"
        label _("Heal All")
        text _("Heal all allies in 30%%.")

    hbox:
        add "gfx/sphere/6.png"
        label _("Attack Up")
        text _("Raise party damage for 1 turn.")

    hbox:
        add "gfx/sphere/7.png"
        label _("Defense Up")
        text _("Raise party defense for 1 turn.")

    null height 32
    hbox:
        label _("Reminder:")
        text _("Spheres goes before Skills.")
    text _("Skills goes before normal attacks.")
    null height 32
    text _("Summoning does not take a turn.")
    text _("Do it before assigning spheres.")


style help_button is gui_button
style help_button_text is gui_button_text
style help_label is gui_label
style help_label_text is gui_label_text
style help_text is gui_text

style help_button:
    properties gui.button_properties("help_button")
    xmargin 8

style help_button_text:
    properties gui.button_text_properties("help_button")

style help_label:
    xsize 250
    right_padding 20

style help_label_text:
    size gui.text_size
    xalign 1.0
    text_align 1.0


########################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Combat Interface
init python:
    ####################### Client rendering
    def c_dragid(dg):
        if dg == "party1":
            return 0
        elif dg == "party2":
            return 1
        elif dg == "party3":
            return 2
        elif dg == "party4":
            return 3
        elif dg == "party5":
            return 4
        else:
            return 0# We can't return -1...

    def combat_action(drags, drop):
        global use_sphere
        if not drop:
            return

        # Define your char slot, and reset the sphere usage for it
        idn=c_dragid(drags[0].drag_name)
        use_sphere[idn]=AP_NONE

        # Check if you asked for a skill
        if (drop.drag_name == "Skill"):
            renpy.notify("You can't use a skill!")
            drags[0].snap(0, 48, delay=0.1)
            #use_sphere[idn]=AP_SKILL
            return

        # Check if you asked for a sphere
        if (drop.drag_name == "Sphere"):
            if (Battle["spheres"][idn]):
                # Mark to use the sphere - if it exists!
                drags[0].snap(0, 96, delay=0.1)
                use_sphere[idn]=AP_SPHERE
                return
            else:
                renpy.notify("You can't use a sphere!")
                drags[0].snap(0, 48, delay=0.1)
                return
        return

    # FIXME: Allow to drag spheres around
    def c_swap(idx):
        global csid
        if csid == None:
            csid = copy.copy(idx)
            return
        o1 = copy.copy(Battle["spheres"][csid])
        o2 = copy.copy(Battle["spheres"][idx])
        Battle["spheres"][csid] = copy.copy(o2)
        Battle["spheres"][idx] = copy.copy(o1)
        csid = None
        return

#############################################################################
screen battle_mobs():
    ####################################################
    # Render enemies
    fixed:
        # Enemy 1
        showif (en1["hp"] > 0):
            add At("mob_"+str(en1["unit_id"]), enemy1, czoom_e1)
            vbox:
                xalign 0.5
                yanchor 0.5
                ypos 0.24
                xmaximum 180
                ymaximum 20
                hbox:
                    add "gfx/gui/ele_%d.png" % en1["ele"]
                    text " %s" % en1["name"]:
                        outlines [(1, "#000", 0, 0)]
                bar value en1["hp"] range en1["max_hp"]

        # Enemy 2
        showif (en2["hp"] > 0):
            add At("mob_"+str(en2["unit_id"]), enemy2, czoom_e2)
            vbox:
                xalign 1.0
                yanchor 0.5
                ypos 0.24
                xmaximum 180
                ymaximum 20
                hbox:
                    add "gfx/gui/ele_%d.png" % en2["ele"]
                    text " %s" % en2["name"]:
                        outlines [(1, "#000", 0, 0)]
                bar value en2["hp"] range en2["max_hp"]

        # Enemy 3
        showif (en3["hp"] > 0):
            add At("mob_"+str(en3["unit_id"]), enemy3, czoom_e3)
            vbox:
                xalign 0.0
                yanchor 0.5
                ypos 0.24
                xmaximum 180
                ymaximum 20
                hbox:
                    add "gfx/gui/ele_%d.png" % en3["ele"]
                    text " %s" % en3["name"]:
                        outlines [(1, "#000", 0, 0)]
                bar value en3["hp"] range en3["max_hp"]

#############################################################################
screen battle_core():
    ####################################################
    # Render allies
    # TODO: Gray out and unmovable if dead
    # One drag group per party member defined in Battle
    fixed:
        draggroup:
            xpos 0.25
            xanchor 1.0
            yanchor 1.0
            ypos config.screen_height-10
            xmaximum 256
            ymaximum 480
            # Display the background
            drag:
                child At("gfx/action.png", party1)
                draggable False
                droppable False
            # Display the card (if there's one)
            if (fx1):
                drag:
                    drag_name "party1"
                    child At(fx1, c_party1, czoom_p1)
                    droppable False
                    if (Battle["party"][0]["hp"] > 0):
                        dragged combat_action
                    else:
                        draggable False
                    ypos 48
                # The action areas
                drag:
                    drag_name "Skill"
                    child At("gfx/actionarea.png", party1)
                    draggable False
                    ypos 0.0
                drag:
                    drag_name "Sphere"
                    child At("gfx/actionarea.png", party1)
                    draggable False
                    yalign 1.0

        if (fx1 and Battle["party"][0]["hp"] <= 0):
            add At("gfx/off.png", party1)

        # Display the sphere
        imagebutton:
            idle ("gfx/sphere/"+str(Battle["spheres"][0])+".png")
            selected_idle ("gfx/sphere/s/"+str(Battle["spheres"][0])+".png")
            hover ("gfx/sphere/"+str(Battle["spheres"][0])+".png")
            selected_hover ("gfx/sphere/s/"+str(Battle["spheres"][0])+".png")
            selected csid == 0
            action Function(c_swap, 0)
            keysym "alt_1"
            xpos 0.125
            xanchor 0.5
            yanchor 1.0
            ypos config.screen_height-10

        # One drag group per party member defined in Battle
        draggroup:
            xpos 0.50
            xanchor 1.0
            yanchor 1.0
            ypos config.screen_height-10
            xmaximum 256
            ymaximum 480
            drag:
                child At("gfx/action.png", party2)
                draggable False
                droppable False
            if (fx2):
                drag:
                    drag_name "party2"
                    child At(fx2, c_party2, czoom_p2)
                    droppable False
                    if (Battle["party"][1]["hp"] > 0):
                        dragged combat_action
                    else:
                        draggable False
                    ypos 48
                drag:
                    drag_name "Skill"
                    child At("gfx/actionarea.png", party2)
                    draggable False
                    ypos 0.0
                drag:
                    drag_name "Sphere"
                    child At("gfx/actionarea.png", party2)
                    draggable False
                    yalign 1.0

        if (fx2 and Battle["party"][1]["hp"] <= 0):
            add At("gfx/off.png", party2)

        imagebutton:
            idle ("gfx/sphere/"+str(Battle["spheres"][1])+".png")
            selected_idle ("gfx/sphere/s/"+str(Battle["spheres"][1])+".png")
            hover ("gfx/sphere/"+str(Battle["spheres"][1])+".png")
            selected_hover ("gfx/sphere/s/"+str(Battle["spheres"][1])+".png")
            selected csid == 1
            action Function(c_swap, 1)
            keysym "alt_2"
            xpos 0.375
            xanchor 0.5
            yanchor 1.0
            ypos config.screen_height-10

        # One drag group per party member defined in Battle
        draggroup:
            xpos 0.75
            xanchor 1.0
            yanchor 1.0
            ypos config.screen_height-10
            xmaximum 256
            ymaximum 480
            drag:
                child At("gfx/action.png", party3)
                draggable False
                droppable False
            if (fx3):
                drag:
                    drag_name "party3"
                    child At(fx3, c_party3, czoom_p3)
                    droppable False
                    if (Battle["party"][2]["hp"] > 0):
                        dragged combat_action
                    else:
                        draggable False
                    ypos 48
                drag:
                    drag_name "Skill"
                    child At("gfx/actionarea.png", party3)
                    draggable False
                    ypos 0.0
                drag:
                    drag_name "Sphere"
                    child At("gfx/actionarea.png", party3)
                    draggable False
                    yalign 1.0

        if (fx3 and Battle["party"][2]["hp"] <= 0):
            add At("gfx/off.png", party3)

        imagebutton:
            idle ("gfx/sphere/"+str(Battle["spheres"][2])+".png")
            selected_idle ("gfx/sphere/s/"+str(Battle["spheres"][2])+".png")
            hover ("gfx/sphere/"+str(Battle["spheres"][2])+".png")
            selected_hover ("gfx/sphere/s/"+str(Battle["spheres"][2])+".png")
            selected csid == 2
            action Function(c_swap, 2)
            keysym "alt_3"
            xpos 0.625
            xanchor 0.5
            yanchor 1.0
            ypos config.screen_height-10

        # One drag group per party member defined in Battle
        draggroup:
            xpos 1.0
            xanchor 1.0
            yanchor 1.0
            ypos config.screen_height-10
            xmaximum 256
            ymaximum 480
            drag:
                child At("gfx/action.png", party4)
                draggable False
                droppable False
            if (fx4):
                drag:
                    drag_name "party4"
                    child At(fx4, c_party4, czoom_p4)
                    droppable False
                    if (Battle["party"][3]["hp"] > 0):
                        dragged combat_action
                    else:
                        draggable False
                    ypos 48
                drag:
                    drag_name "Skill"
                    child At("gfx/actionarea.png", party4)
                    draggable False
                    ypos 0.0
                drag:
                    drag_name "Sphere"
                    child At("gfx/actionarea.png", party4)
                    draggable False
                    yalign 1.0

        if (fx4 and Battle["party"][3]["hp"] <= 0):
            add At("gfx/off.png", party4)

        imagebutton:
            idle ("gfx/sphere/"+str(Battle["spheres"][3])+".png")
            selected_idle ("gfx/sphere/s/"+str(Battle["spheres"][3])+".png")
            hover ("gfx/sphere/"+str(Battle["spheres"][3])+".png")
            selected_hover ("gfx/sphere/s/"+str(Battle["spheres"][3])+".png")
            selected csid == 3
            action Function(c_swap, 3)
            keysym "alt_4"
            xpos 0.875
            xanchor 0.5
            yanchor 1.0
            ypos config.screen_height-10

#############################################################################
screen battle_bars():
    ####################################################
    # Render HPBARs
    fixed:
        showif (fx1):
            frame:
                xpos 0.125
                xanchor 0.5
                yanchor 1.0
                ypos 1.0
                ymaximum 10
                xmaximum 256
                bar value Battle["party"][0]["hp"] range Battle["party"][0]["max_hp"] xmaximum 256

        showif (fx2):
            frame:
                xpos 0.375
                xanchor 0.5
                yanchor 1.0
                ypos 1.0
                ymaximum 10
                xmaximum 256
                bar value Battle["party"][1]["hp"] range Battle["party"][1]["max_hp"] xmaximum 256

        showif (fx3):
            frame:
                xpos 0.625
                xanchor 0.5
                yanchor 1.0
                ypos 1.0
                ymaximum 10
                xmaximum 256
                bar value Battle["party"][2]["hp"] range Battle["party"][2]["max_hp"] xmaximum 256

        showif (fx4):
            frame:
                xpos 0.875
                xanchor 0.5
                yanchor 1.0
                ypos 1.0
                ymaximum 10
                xmaximum 256
                bar value Battle["party"][3]["hp"] range Battle["party"][3]["max_hp"] xmaximum 256

#############################################################################
screen battle():
    # Background and append all three battle sub-screens
    # FIXME Our custom displayables are not 100.00% reliable
    add "bg %s" % TMP_BACKG
    use battle_mobs
    use battle_core
    use battle_bars

    ####################################################
    # Render HUD
    frame:
        xalign 0.0
        yalign 0.0
        xfill True
        ymaximum 60
        yfill True
        background Frame("gui/frame.png", 0, 0)
    fixed:
        ymaximum 60
        hbox:
            xalign 0.0
            yalign 0.0
            # Preferences button
            null width 20
            imagebutton auto "gfx/gui/cog_%s.png" action ShowMenu('sumconf')
            null width 20
            text "%d " % (Battle["turn"])
            text _(" Turn")
        hbox:
            xalign 0.5
            yalign 0.0
            text _("Wave %d/%d" % (Battle["wave"], Battle["max_wave"]))
        showif not btl_ready:
            hbox:
                xalign 1.0
                yalign 0.0
                button:
                    text _("I'm ready"):
                        outlines [(1,"#333",0,0)]
                    action Return(ACT_TURN)
        showif not btl_ready and Player["quest"] >= 5:
            hbox:
                xanchor 1.0
                yanchor 0.0
                xpos config.screen_width-32
                ypos 96
                imagebutton:
                    insensitive "gfx/gui/summon_none.png"
                    idle "gfx/gui/summon_idle.png"
                    hover "gfx/gui/summon_hover.png"
                    action ifte(Battle["bp"] >= allsummons[persistent.summon]["cost"], Return(ACT_SUMMON), None)




########################################################################################
#     This file is part of Spheres.
#     Copyright (C) 2019  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Definitions - Music, Graphics, and JSON data
############################################################################
init python:
    def logindata():
        global password
        # TODO: Obtain user id based on device
        return """{
                "passwd": "%s",
                "version": "%s"
                }""" % (password, config.version)


    def recruitdata(t, a):
        return """{
                "tavern": %d,
                "amount": %d,
                "packet": %d
                }""" % (t, a, now())


    def questdata(w, q, p):
        return """{
                "world": "%s",
                "quest_id": %d,
                "party_id": %d,
                "packet": %d
                }""" % (w, q, p, now())


    def battledata(u, s, a):
        return """{
                "unit": %s,
                "sphere": %s,
                "action": %s,
                "packet": %d
                }""" % (json.dumps(u), json.dumps(s), json.dumps(a), now())

    def square_composite(cid, path, ei=False):
        global allunits
        # We need to try to get rarity
        try:
            r=allunits[int(cid)]["rare"]
        except:
            r=8 # allunits probably unitialized

        # We need to try to get the element
        try:
            e=allunits[int(cid)]["attribute"]
        except:
            e=0

        if not ei:
            return Composite(
                (340, 340),
                (0, 0), "gfx/square/bg.png",
                (0, 0), path,
                (0, 0), "gfx/square/%d.png" % r,
                (0, 0), "gfx/square/ele_%d.png" % e)
        else:
            return Composite(
                (340, 340),
                (0, 0), "gfx/square/bg.png",
                (0, 0), ExtraImage(path),
                (0, 0), "gfx/square/%d.png" % r,
                (0, 0), "gfx/square/ele_%d.png" % e)

    def card_composite(cid, path, ei=False):
        global allunits
        # We need to try to get rarity
        try:
            r=allunits[int(cid)]["rare"]
        except:
            r=8 # allunits probably unitialized

        # We need to try to get the element
        try:
            e=allunits[int(cid)]["attribute"]
            print(str(e))
        except:
            e=0

        if not ei:
            return Composite(
            (640, 960),
            (0, 0), "gfx/cards/bg.png",
            (0, 0), path,
            (0, 0), "gfx/cards/"+str(r)+".png",
            (0, 0), "gfx/cards/ele/"+str(e)+".png")
        else:
            return Composite(
            (640, 960),
            (0, 0), "gfx/cards/bg.png",
            (0, 0), ExtraImage(path),
            (0, 0), "gfx/cards/"+str(r)+".png",
            (0, 0), "gfx/cards/ele/"+str(e)+".png")
        # size,)

    # TODO: square_ and gfx/square/units
    # Converts regular image filepaths to displayables names and vice-versa
    def img_regex(name, reverse=False):
        if not reverse:
            # Extension is ommited, add them yourself!
            if name.startswith("unit_"):
                return "gfx/units/%s" % name.replace("unit_")
            elif name.startswith("mob_"):
                return "gfx/mobs/%s" % name.replace("mob_")
            elif name.startswith("dialog_"):
                return "gfx/dialog/%s" % name.replace("dialog_")
            elif name.startswith("bg "):
                return "gfx/bg/%s" % name.replace("bg ")
            elif name.startswith("summon_"):
                return "gfx/summons/%s" % name.replace("summon_")
            else:
                print("ERROR: Not a regular filename")
                return name
        else:
            if name.startswith("gfx/units/"):
                return "unit_%s" % name
            elif name.startswith("gfx/mobs/"):
                return "mob_%s" % name
            elif name.startswith("gfx/dialog/"):
                return "dialog_%s" % name
            elif name.startswith("gfx/bg/"):
                return "bg %s" % name
            elif name.startswith("gfx/summons/"):
                return "summon_%s" % name
            else:
                print("ERROR: Not a regular display name")
                return name

    # Loads a sound called VAL
    def get_sfx(val, ext=".mp3"):
        # Search for the sound
        show_img(val, False, ext=ext)

        try:
            valb=dl_search(persistent.allfiles, 0, val)[1]
        except:
            traceback.print_exc()
            valb = ERR_INVALID
        if valb == ERR_INVALID:
            print("Invalid Sound: %s" % (val))
            return RetString("sfx/regnum.mp3").id() # FIXME
        return RetString(valb).id()


    # Load sfx (or rather, register the path)
    for file in renpy.list_files():
        fn=file.replace('sfx/','').replace('/', ' ').replace('.mp3','').replace('.ogg','')
        if file.startswith('sfx/'):
            if file.endswith('.mp3') or file.endswith('.ogg'):
                if not fn in persistent.allfiles:
                    persistent.allfiles.append((fn, file))
                allfiles.append(fn)
                continue
            continue


    # Load sprite images: "unit" (XXX: Unitialized - rarity not shown)
    for file in renpy.list_files():
        fn=file.replace('gfx/units/','').replace('/', ' ').replace('.png','').replace('.webp','')
        if file.startswith('gfx/units/'):
            if file.endswith('.png') or file.endswith('.webp'):
                name = "unit_"+fn
                #renpy.image(name, Image(file, yanchor=1.0))
                renpy.image(name, card_composite(fn, file))

                dl=dl_search(persistent.allfiles, 0, name)
                if dl is ERR_INVALID:
                    persistent.allfiles.append((name, file))
                allfiles.append(name)
                continue
            continue

    # Load sprite images: "mob"
    for file in renpy.list_files():
        fn=file.replace('gfx/mobs/','').replace('/', ' ').replace('.png','').replace('.webp','')
        if file.startswith('gfx/mobs/'):
            if file.endswith('.png') or file.endswith('.webp'):
                name = "mob_"+fn
                renpy.image(name, Image(file, yanchor=1.0))
                if not name in persistent.allfiles:
                    persistent.allfiles.append((name, file))
                allfiles.append(name)
                continue
            continue


    # Load sprite images: "dialog"
    for file in renpy.list_files():
        fn=file.replace('gfx/dialog/','').replace('/', ' ').replace('.png','').replace('.webp','')
        if file.startswith('gfx/dialog/'):
            if file.endswith('.png') or file.endswith('.webp'):
                name = "dialog_"+fn
                renpy.image(name, Image(file, yanchor=1.0))
                dl=dl_search(persistent.allfiles, 0, name)
                if dl is ERR_INVALID:
                    persistent.allfiles.append((name, file))
                allfiles.append(name)
                continue
            continue


    # Load background images: "bg"
    for file in renpy.list_files():
        fn=file.replace('gfx/bg/','').replace('/', ' ').replace('.png','').replace('.webp','')
        if file.startswith('gfx/bg/'):
            if file.endswith('.png') or file.endswith('.webp'):
                name = "bg "+fn
                renpy.image(name, Frame(file, 0, 0))
                dl=dl_search(persistent.allfiles, 0, name)
                if dl is ERR_INVALID:
                    persistent.allfiles.append((name, file))
                allfiles.append(name)
                continue
            continue


    # Load summon images: "summon"
    for file in renpy.list_files():
        fn=file.replace('gfx/summons/','').replace('/', ' ').replace('.png','').replace('.webp','')
        if file.startswith('gfx/summons/'):
            if file.endswith('.png') or file.endswith('.webp'):
                name = "summon_"+fn
                renpy.image(name, Image(file, yanchor=1.0))
                dl=dl_search(persistent.allfiles, 0, name)
                if dl is ERR_INVALID:
                    persistent.allfiles.append((name, file))
                allfiles.append(name)
                continue
            continue


    # Load summon images: "square" (XXX: Unitialized - rarity not shown)
    for file in renpy.list_files():
        fn=file.replace('gfx/square/','').replace('/', ' ').replace('.png','').replace('.webp','')
        if file.startswith('gfx/square/'):
            if file.endswith('.png') or file.endswith('.webp'):
                name = "square_"+fn
                #renpy.image(name, Image(file, yanchor=1.0))
                renpy.image(name, square_composite(fn, file))
                dl=dl_search(persistent.allfiles, 0, name)
                if dl is ERR_INVALID:
                    persistent.allfiles.append((name, file))
                allfiles.append(name)
                continue
            continue

    # TODO: Maybe load images on persistent.allfiles now?
    # I suspect declaring images at runtime might have a memleak
    # Needs investigation


    def star_write(am):
        i, st = 0, ""
        while i < am:
            i+=1
            st+="★"
        return st


    # Overrides renpy.image() method
    def new_img(name, where, isimage=True):
        # d: Downloaded path
        if not isinstance(name, tuple):
            name = tuple(name.split())

        #d = renpy.renpy.easy.displayable(where)
        if isimage:
            d=ExtraImage(where)
        else:
            d = renpy.renpy.easy.displayable(where)
        renpy.renpy.display.image.register_image(name, d)
        return

    # Retrieves Ren'Py displayable name associated to PATH
    def get_img(path):
        # Search for the image name
        val=img_regex(path, True)
        show_img(val, False)

        valb=dl_search(persistent.allfiles, 0, val)[1]
        if valb == ERR_INVALID:
            print("Invalid Image: %s (%s)" % (path, val))
            return "gfx/spinner.png"
        return valb

    # Overrides renpy.show() and renpy.image() methods
    # Missing: transient=False, munge_name=True
    # FIXME: Does not saves properly in Windows
    def show_img(img, show=True, at_list=[ ], tag=None, zorder=None, behind=[ ], atl=None, what=None, layer=None, ext=".webp"):
        global tr_loading
        # Image exists, display it
        if img in allfiles:
            if show:
                renpy.show(img, at_list=at_list, tag=tag, zorder=zorder, behind=behind, atl=atl, what=what, layer=layer)
            return

        # Have we downloaded this image previously?
        path=dl_search(persistent.allfiles, 0, img)
        print(str(path))

        # Image doesn't exists, we must download it
        while (path == ERR_INVALID):
            tr_loading=True
            # Latest version converts these formats to WebP
            if ext in [".png", ".jpg", ".jpeg"]:
                ext=".webp"
            # Otherwise, preserve extension.
            if True or renpy.android:
                addr="extra_%s%s" % (img.replace(" ", "_"), ext)
            else:
                addr="extra/%s%s" % (img.replace(" ", "_"), ext)

            f=open(get_path(addr), "wb")
            stdout("Downloading additional file: %s" % img.replace(" ", "_"))
            try:
                x=requests.get("%s://%s/assets/%s?token=%s" % (ifte(persistent.ssl_enabled, "https", "http"), HOST, img.replace(" ", "_"), get_token()), verify=False) # , timeout=8.0 → Need to handle sudden death
            except:
                # Oh noes - something went *terribly* wrong
                traceback.print_exc()
                x=requests.Response()
                x.status_code=403

            if x.status_code == 200:
                f.write(x.content)
                f.close()
                # Android needs paths to be saved by full
                # But audio hates it, so.
                if ext not in [".mp3", ".ogg"]:
                    addr=get_path(addr)
                path=((img, addr))
                persistent.allfiles.append(path)
            else:
                stdout("ERROR FOR: %s://%s/assets/%s?token=%s" % (ifte(persistent.ssl_enabled, "https", "http"), HOST, img.replace(" ", "_"), get_token()))
                try:
                    retry=renpy.call_screen("confirm", "Error downloading file.\nError Code: %d\n\nRetry?" % x.status_code, Return(True), Return(False))
                    if not retry:
                        if tag is None:
                            tag=img
                        path=None
                        if show:
                            if tag is None:
                                tag=img
                            print("tag is %s" % str(tag))
                            renpy.show("spinner", at_list=at_list, tag=tag, zorder=zorder, behind=behind, atl=atl, what=what, layer=layer) # TODO Show error
                        tr_loading=False
                        return
                    # TODO: “Retry?”
                except:
                    print("Failed, trying again in half second...")
                    time.sleep(0.5)

        # Image exists, but wasn't loaded yet
        if (path != ERR_INVALID and path is not None):
            print("Detected not loaded image: %s" % path[0])
            # Valid Image Extensions: PNG, JPG, JPEG, GIF, WEBP
            if ext in [".png", ".jpg", ".jpeg", ".gif", ".webp"]:
                # Maybe it is an unit
                if img.startswith("unit_"):
                    new_img(img, card_composite(img.replace("unit_", ""), path[1], ei=True), isimage=False)
                elif img.startswith("square_"):
                    new_img(img, square_composite(img.replace("square_", ""), path[1], ei=True), isimage=False)
                else:
                    new_img(img, path[1])

            stdout("registered image: "+path[1])
            allfiles.append(img)
            if show:
                renpy.show(img, at_list=at_list, tag=tag, zorder=zorder, behind=behind, atl=atl, what=what, layer=layer)
            tr_loading=False
            return

        # Something went wrong
        stdout("show_img reached abnormal ending")
        return

    ##########################################################
    # Other Music
    #MUSIC_OPENING => Init Block -3
    #MUSIC_TOWN => Init Block -3
    MUSIC_BATTLE=RetString("sfx/bgm03.mp3")
    MUSIC_BOSS=RetString("sfx/bgm04.mp3")
    MUSIC_PARTY=RetString("sfx/bgm05.ogg")
    MUSIC_VICTORY=RetString("sfx/bgm06.mp3")
    MUSIC_WORLDMAP=RetString("sfx/bgm07.mp3")
    #MUSIC_WORLDMAP=ExecuteOnCall(get_sfx, "sfx_bgm07", ".mp3")
    MUSIC_PROLOGUE01=RetString("sfx/regnum.mp3")
    MUSIC_PROLOGUE02=RetString("sfx/prologue.mp3")
    MUSIC_PROLOGUE03=RetString("sfx/shining.mp3")

init 1 python:
    def unitsquare(uid, atl=czoom_70):
        show_img("square_%d" % uid, False)
        try:
            btn=At("square_%d" % uid, atl)
        except:
            btn=At("square_0", atl)
            traceback.print_exc()
            stdout("[ERROR] ATCODE FOR square_%d is INVALID" % uid)
        return btn

